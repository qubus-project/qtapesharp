using System;
using System.Threading;
using System.Threading.Tasks;
using QTapeSharp;
using System.Diagnostics;
using System.IO;
using NUnit.Framework;

namespace Tests
{
    public sealed class ConsistencyTest : IAsyncDisposable
    {
        public static ConsistencyTest Start(string path)
        {
            QTape tapeFile = QTape.Open(path, CancellationToken.None).AsTask().Result;
            
            return new ConsistencyTest(path, tapeFile);
        }

        public ValueTask DisposeAsync()
        {
            return this.tapeFile.DisposeAsync();
        }

        public ConsistencyTest Check(Func<QTape, ValueTask> action)
        {
            RunCheck(action).Wait();
            
            return this;
        }
        
        private ConsistencyTest(string path, QTape tapeFile)
        {
            this.path = path;
            this.tapeFile = tapeFile;
        }

        private async Task RunCheck(Func<QTape, ValueTask> action)
        {
            await action(this.tapeFile).ConfigureAwait(false);

            await CheckConsistency();
        }

        private async ValueTask CheckConsistency()
        {
            try
            {
                await using QTape intermediateTape = await QTape.Open(this.path, CancellationToken.None);
                
                await foreach (Track track in intermediateTape.QueryAllTracks())
                {
                    await foreach (Event e in track.QueryEverything())
                    {
                    }
                }
            }
            catch (QTapeSharp.Exception e)
            {
                Assert.Fail("The persistent version of the tested QTape file is inconsistent.");
            }
        }

        private readonly string path;
        private readonly QTape tapeFile;
    }
}
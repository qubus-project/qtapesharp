using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using NUnit.Framework;
using System.Collections.Generic;
using QTapeSharp;

namespace Tests
{
    public sealed class QTapeTest
    {
        /// <summary>
        ///     Creates a new temporary directory.
        /// </summary>
        /// <returns>The path of the temporary directory.</returns>
        public static string CreateTempDirectory()
        {
            // Loop until we have found a non-existing directory name within the operation system'
            // directory for temporaries.
            while (true)
            {
                string path = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

                // Check if the path exists and create the directory if it does not.
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                    return path;
                }
            }
        }

        private string? tempDir = null;

        [SetUp]
        public void Setup()
        {
            this.tempDir = CreateTempDirectory();
        }

        [TearDown]
        public void Teardown()
        {
            Directory.Delete(this.tempDir, true);
        }

        [Test]
        public async Task PrimitiveIntTest()
        {
            var events = new List<Event<int>>
            {
                new Event<int>(new DateTime(2020, 1, 1, 0, 0, 0, 0), 1),
                new Event<int>(new DateTime(2020, 1, 1, 0, 0, 0, 1), 2),
                new Event<int>(new DateTime(2020, 1, 1, 0, 0, 0, 2), 3),
                new Event<int>(new DateTime(2020, 1, 1, 0, 0, 0, 3), 4),
                new Event<int>(new DateTime(2020, 1, 1, 0, 0, 0, 4), 5),
                new Event<int>(new DateTime(2020, 1, 1, 0, 0, 0, 5), 6),
                new Event<int>(new DateTime(2020, 1, 1, 0, 0, 0, 6), 7)
            };

            {
                await using var tape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None).ConfigureAwait(false);

                Track<int> track = await tape.AddTrack<int>("foo", CancellationToken.None).ConfigureAwait(false);

                foreach (Event<int> ev in events)
                {
                    await track.AppendEvent(ev.Timestamp, ev.Payload, CancellationToken.None).ConfigureAwait(false);
                }
            }

            await using var rereadTape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None).ConfigureAwait(false);

            Track<int> writtenTrack = await rereadTape.OpenTrack<int>("foo", CancellationToken.None).ConfigureAwait(false);

            var writtenEvents = new List<Event<int>>();

            await foreach (Event<int> ev in writtenTrack.QueryEverything().ConfigureAwait(false))
            {
                writtenEvents.Add(ev);
            }

            CollectionAssert.AreEqual(events, writtenEvents);
        }

        [Test]
        public async Task PrimitiveLongTest()
        {
            var events = new List<Event<long>>
            {
                new Event<long>(new DateTime(2020, 1, 1, 0, 0, 0, 0), 1),
                new Event<long>(new DateTime(2020, 1, 1, 0, 0, 0, 1), 2),
                new Event<long>(new DateTime(2020, 1, 1, 0, 0, 0, 2), 3),
                new Event<long>(new DateTime(2020, 1, 1, 0, 0, 0, 3), 4),
                new Event<long>(new DateTime(2020, 1, 1, 0, 0, 0, 4), 5),
                new Event<long>(new DateTime(2020, 1, 1, 0, 0, 0, 5), 6),
                new Event<long>(new DateTime(2020, 1, 1, 0, 0, 0, 6), 7)
            };

            {
                await using var tape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None).ConfigureAwait(false);

                Track<long> track = await tape.AddTrack<long>("foo", CancellationToken.None).ConfigureAwait(false);

                foreach (Event<long> ev in events)
                {
                    await track.AppendEvent(ev.Timestamp, ev.Payload, CancellationToken.None).ConfigureAwait(false);
                }
            }

            await using var rereadTape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None).ConfigureAwait(false);

            Track<long> writtenTrack = await rereadTape.OpenTrack<long>("foo", CancellationToken.None).ConfigureAwait(false);

            var writtenEvents = new List<Event<long>>();

            await foreach (Event<long> ev in writtenTrack.QueryEverything().ConfigureAwait(false))
            {
                writtenEvents.Add(ev);
            }

            CollectionAssert.AreEqual(events, writtenEvents);
        }

        [Test]
        public async Task PrimitiveFloatTest()
        {
            var events = new List<Event<float>>
            {
                new Event<float>(new DateTime(2020, 1, 1, 0, 0, 0, 0), 1),
                new Event<float>(new DateTime(2020, 1, 1, 0, 0, 0, 1), 2),
                new Event<float>(new DateTime(2020, 1, 1, 0, 0, 0, 2), 3),
                new Event<float>(new DateTime(2020, 1, 1, 0, 0, 0, 3), 4),
                new Event<float>(new DateTime(2020, 1, 1, 0, 0, 0, 4), 5),
                new Event<float>(new DateTime(2020, 1, 1, 0, 0, 0, 5), 6),
                new Event<float>(new DateTime(2020, 1, 1, 0, 0, 0, 6), 7)
            };

            {
                await using var tape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None).ConfigureAwait(false);

                Track<float> track = await tape.AddTrack<float>("foo", CancellationToken.None).ConfigureAwait(false);

                foreach (Event<float> ev in events)
                {
                    await track.AppendEvent(ev.Timestamp, ev.Payload, CancellationToken.None).ConfigureAwait(false);
                }
            }

            await using var rereadTape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None).ConfigureAwait(false);

            Track<float> writtenTrack = await rereadTape.OpenTrack<float>("foo", CancellationToken.None);

            var writtenEvents = new List<Event<float>>();

            await foreach (Event<float> ev in writtenTrack.QueryEverything())
            {
                writtenEvents.Add(ev);
            }

            CollectionAssert.AreEqual(events, writtenEvents);
        }

        [Test]
        public async Task PrimitiveDoubleTest()
        {
            var events = new List<Event<double>>
            {
                new Event<double>(new DateTime(2020, 1, 1, 0, 0, 0, 0), 1),
                new Event<double>(new DateTime(2020, 1, 1, 0, 0, 0, 1), 2),
                new Event<double>(new DateTime(2020, 1, 1, 0, 0, 0, 2), 3),
                new Event<double>(new DateTime(2020, 1, 1, 0, 0, 0, 3), 4),
                new Event<double>(new DateTime(2020, 1, 1, 0, 0, 0, 4), 5),
                new Event<double>(new DateTime(2020, 1, 1, 0, 0, 0, 5), 6),
                new Event<double>(new DateTime(2020, 1, 1, 0, 0, 0, 6), 7)
            };

            {
                await using var tape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None);

                Track<double> track = await tape.AddTrack<double>("foo", CancellationToken.None);

                foreach (Event<double> ev in events)
                {
                    await track.AppendEvent(ev.Timestamp, ev.Payload, CancellationToken.None);
                }
            }

            await using var rereadTape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None);

            Track<double> writtenTrack = await rereadTape.OpenTrack<double>("foo", CancellationToken.None);

            var writtenEvents = new List<Event<double>>();

            await foreach (Event<double> ev in writtenTrack.QueryEverything())
            {
                writtenEvents.Add(ev);
            }

            CollectionAssert.AreEqual(events, writtenEvents);
        }
        
        [Test]
        public async Task PrimitiveDecimaöTest()
        {
            var events = new List<Event<decimal>>
                         {
                             new Event<decimal>(new DateTime(2020, 1, 1, 0, 0, 0, 0), 1),
                             new Event<decimal>(new DateTime(2020, 1, 1, 0, 0, 0, 1), 2),
                             new Event<decimal>(new DateTime(2020, 1, 1, 0, 0, 0, 2), 3),
                             new Event<decimal>(new DateTime(2020, 1, 1, 0, 0, 0, 3), 4),
                             new Event<decimal>(new DateTime(2020, 1, 1, 0, 0, 0, 4), 5),
                             new Event<decimal>(new DateTime(2020, 1, 1, 0, 0, 0, 5), 6),
                             new Event<decimal>(new DateTime(2020, 1, 1, 0, 0, 0, 6), 7)
                         };

            {
                await using var tape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None);

                Track<decimal> track = await tape.AddTrack<decimal>("foo", CancellationToken.None);

                foreach (Event<decimal> ev in events)
                {
                    await track.AppendEvent(ev.Timestamp, ev.Payload, CancellationToken.None);
                }
            }

            await using var rereadTape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None);

            Track<decimal> writtenTrack = await rereadTape.OpenTrack<decimal>("foo", CancellationToken.None);

            var writtenEvents = new List<Event<decimal>>();

            await foreach (Event<decimal> ev in writtenTrack.QueryEverything())
            {
                writtenEvents.Add(ev);
            }

            CollectionAssert.AreEqual(events, writtenEvents);
        }

        [Test]
        public async Task TestOpenOrCreate()
        {
            await using var tape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None);

            Track<int> trackv1 = await tape.OpenOrCreateTrack<int>("foo", CancellationToken.None);
            
            Track<int> trackv2 = await tape.OpenOrCreateTrack<int>("foo", CancellationToken.None);

            await trackv2.AppendEvent(new DateTime(2020, 05, 1, 12, 0, 0), 42, CancellationToken.None);
            await trackv1.AppendEvent(new DateTime(2020, 05, 1, 12, 0, 1), 7, CancellationToken.None);

            IAsyncEnumerator<Event<int>> recordedEvents = trackv1.QueryEverything().GetAsyncEnumerator();

            await recordedEvents.MoveNextAsync();
            
            Assert.AreEqual(new DateTime(2020, 05, 1, 12, 0, 0), recordedEvents.Current.Timestamp);
            Assert.AreEqual(42, recordedEvents.Current.Payload);
            
            await recordedEvents.MoveNextAsync();
            
            Assert.AreEqual(new DateTime(2020, 05, 1, 12, 0, 1), recordedEvents.Current.Timestamp);
            Assert.AreEqual(7, recordedEvents.Current.Payload);

            Assert.IsFalse(await recordedEvents.MoveNextAsync());
        }

        [Test]
        public async Task TestDispose_TheQTapeHasAlreadyBeenDisposed_NoExceptionIsThrown()
        {
            var tape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None);

            await tape.DisposeAsync();
            
            Assert.DoesNotThrowAsync(async () => await tape.DisposeAsync());
        }

        [Test]
        public async Task LargeTrackTest()
        {
            var events = new List<Event<long>>();

            {
                await using var tape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None);

                Track<long> track = await tape.AddTrack<long>("foo", CancellationToken.None);

                for (long i = 0; i < 1000; ++i)
                {
                    events.Add(new Event<long>(new DateTime(i), i));
                    await track.AppendEvent(new DateTime(i), i, CancellationToken.None);
                }
            }

            await using var rereadTape = await QTape.Open(Path.Join(this.tempDir, "intTest.qtape"), CancellationToken.None);

            Track<long> writtenTrack = await rereadTape.OpenTrack<long>("foo", CancellationToken.None);

            var writtenEvents = new List<Event<long>>();

            await foreach (Event<long> ev in writtenTrack.QueryEverything())
            {
                writtenEvents.Add(ev);
            }

            CollectionAssert.AreEqual(events, writtenEvents);
        }

        [Test]
        public async Task TestConsistency()
        {
            Track<int>? testTrack = null;

            var currentDate = new DateTime(2020, 12, 3, 12, 43, 22);

            await using var test = ConsistencyTest.Start(Path.Join(this.tempDir, "consistencyTest.qtape"))
                           .Check(async tapeFile =>
                                  {
                                      testTrack = await tapeFile.AddTrack<int>("foo", CancellationToken.None);
                                  })
                           .Check(tapeFile => testTrack.AppendEvent(currentDate, 42, CancellationToken.None))
                           .Check(async tapeFile =>
                                  {
                                      for (int i = 0; i < 100000; ++i)
                                      {
                                          currentDate = currentDate.AddSeconds(1);

                                          await testTrack.AppendEvent(currentDate, 42, CancellationToken.None);
                                      }
                                  })
                           .Check(tapeFile => tapeFile.Flush(CancellationToken.None))
                           .Check(async tapeFile =>
                                  {
                                      for (int i = 0; i < 100000; ++i)
                                      {
                                          currentDate = currentDate.AddSeconds(1);

                                          await testTrack.AppendEvent(currentDate, 42, CancellationToken.None);
                                      }
                                  });
        }
    }
}
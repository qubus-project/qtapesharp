# QTapeSharp

The official C# implementation of the QTape (vocalized Cue Tape) file format.

QTape is a container format optimized for storing application traces, e.g.,
performance metrics and events. Each track of the container can house an
independent user-defined stream of data. A temporal index allowsto query
a certain point in time without loading the entire container into main memory.
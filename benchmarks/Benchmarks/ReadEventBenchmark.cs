using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using QTapeSharp;

namespace Benchmarks
{
    [SimpleJob()]
    [MemoryDiagnoser]
    public class ReadEventBenchmark
    {
        /// <summary>
        ///     Creates a new temporary directory.
        /// </summary>
        /// <returns>The path of the temporary directory.</returns>
        public static string CreateTempDirectory()
        {
            // Loop until we have found a non-existing directory name within the operation system'
            // directory for temporaries.
            while (true)
            {
                string path = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

                // Check if the path exists and create the directory if it does not.
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                    return path;
                }
            }
        }

        private string        path;
        private QTape         tape;
        private Track<long>   int64Track;
        private Track<int>    int32Track;
        private Track<float>  floatTrack;
        private Track<double> doubleTrack;

        private IAsyncEnumerator<Event<long>>   int64Events;
        private IAsyncEnumerator<Event<int>>    int32Events;
        private IAsyncEnumerator<Event<float>>  floatEvents;
        private IAsyncEnumerator<Event<double>> doubleEvents;

        [GlobalSetup]
        public void Setup()
        {
            this.path = CreateTempDirectory();
            this.tape = QTape.Open(Path.Join(this.path, "benchmark.qtape"), CancellationToken.None).Result;

            this.int64Track  = this.tape.AddTrack<long>("int64", CancellationToken.None).Result;
            this.int32Track  = this.tape.AddTrack<int>("int32", CancellationToken.None).Result;
            this.floatTrack  = this.tape.AddTrack<float>("float", CancellationToken.None).Result;
            this.doubleTrack = this.tape.AddTrack<double>("double", CancellationToken.None).Result;

            for (int i = 0; i < 100000; ++i)
            {
                this.int64Track.AppendEvent(new DateTime(i), i, CancellationToken.None).AsTask().Wait();
                this.int32Track.AppendEvent(new DateTime(i), i, CancellationToken.None).AsTask().Wait();
                this.floatTrack.AppendEvent(new DateTime(i), i, CancellationToken.None).AsTask().Wait();
                this.doubleTrack.AppendEvent(new DateTime(i), i, CancellationToken.None).AsTask().Wait();
            }

            this.int64Events  = this.int64Track.QueryEverything().GetAsyncEnumerator();
            this.int32Events  = this.int32Track.QueryEverything().GetAsyncEnumerator();
            this.floatEvents  = this.floatTrack.QueryEverything().GetAsyncEnumerator();
            this.doubleEvents = this.doubleTrack.QueryEverything().GetAsyncEnumerator();
        }

        [GlobalCleanup]
        public void Cleanup()
        {
            this.int64Events.DisposeAsync().AsTask().Wait();

            this.tape.DisposeAsync().AsTask().Wait();
            Directory.Delete(this.path, true);
        }

        [Benchmark]
        public async ValueTask<Event<long>> ReadInt64Event()
        {
            var ev = this.int64Events.Current;

            if (!await this.int64Events.MoveNextAsync().ConfigureAwait(false))
            {
                this.int64Events = this.int64Track.QueryEverything().GetAsyncEnumerator();
            }

            return ev;
        }

        [Benchmark]
        public async ValueTask<Event<int>> ReadInt32Event()
        {
            var ev = this.int32Events.Current;

            if (!await this.int32Events.MoveNextAsync().ConfigureAwait(false))
            {
                this.int32Events = this.int32Track.QueryEverything().GetAsyncEnumerator();
            }

            return ev;
        }

        [Benchmark]
        public async ValueTask<Event<float>> ReadFloatEvent()
        {
            var ev = this.floatEvents.Current;

            if (!await this.floatEvents.MoveNextAsync().ConfigureAwait(false))
            {
                this.floatEvents = this.floatTrack.QueryEverything().GetAsyncEnumerator();
            }

            return ev;
        }

        [Benchmark]
        public async ValueTask<Event<double>> ReadDoubleEvent()
        {
            var ev = this.doubleEvents.Current;

            if (!await this.doubleEvents.MoveNextAsync().ConfigureAwait(false))
            {
                this.doubleEvents = this.doubleTrack.QueryEverything().GetAsyncEnumerator();
            }

            return ev;
        }
    }
}

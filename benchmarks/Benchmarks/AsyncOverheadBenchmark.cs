using System;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
    [SimpleJob()]
    [MemoryDiagnoser]
    public class AsyncOverheadBenchmark
    {
        [Benchmark]
        public async ValueTask<int> ProduceInt32ValueLazyAsync()
        {
            await Task.Yield();
            
            return 42;
        }
        
        [Benchmark]
        public async ValueTask<int> ProduceInt32ValueAsync()
        {
            return 42;
        }
        
        [Benchmark(Baseline = true)]
        public int ProduceInt32Value()
        {
            return 42;
        }
    }
}

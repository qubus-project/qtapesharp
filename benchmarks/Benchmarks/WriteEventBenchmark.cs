using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using QTapeSharp;

namespace Benchmarks
{
    [SimpleJob()]
    [MemoryDiagnoser]
    public class WriteEventBenchmark
    {
        /// <summary>
        ///     Creates a new temporary directory.
        /// </summary>
        /// <returns>The path of the temporary directory.</returns>
        public static string CreateTempDirectory()
        {
            // Loop until we have found a non-existing directory name within the operation system'
            // directory for temporaries.
            while (true)
            {
                string path = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

                // Check if the path exists and create the directory if it does not.
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                    return path;
                }
            }
        }

        private string      path;
        private QTape       tape;
        private Track<long> int64Track;
        private Track<int>  int32Track;
        private Track<long> floatTrack;
        private Track<int>  doubleTrack;

        private FileStream   csvFile;
        private StreamWriter csvWriter;

        private DateTime csvTestDate;
        private int      csvTestData;

        private const    int    bufferSize       = 4096;
        private int    currentBufferPos = 0;
        private readonly byte[] buffer           = new byte[bufferSize];

        [GlobalSetup]
        public void Setup()
        {
            this.path = CreateTempDirectory();
            this.tape = QTape.Open(Path.Join(this.path, "benchmark.qtape"), CancellationToken.None).Result;

            this.int64Track  = this.tape.AddTrack<long>("int64", CancellationToken.None).Result;
            this.int32Track  = this.tape.AddTrack<int>("int32", CancellationToken.None).Result;
            this.floatTrack  = this.tape.AddTrack<long>("float", CancellationToken.None).Result;
            this.doubleTrack = this.tape.AddTrack<int>("double", CancellationToken.None).Result;

            this.csvFile   = new FileStream("benchmark.csv", FileMode.Create);
            this.csvWriter = new StreamWriter(this.csvFile);

            this.csvTestDate = new DateTime(2020, 5, 26, 7, 7, 32);
            this.csvTestData = 1234567890;
        }

        [GlobalCleanup]
        public void Cleanup()
        {
            this.csvWriter.Dispose();
            this.csvFile.Dispose();

            this.tape.DisposeAsync().AsTask().Wait();
            Directory.Delete(this.path, true);
        }

        [Benchmark]
        public ValueTask AppendInt64Event() => this.int64Track.AppendEvent(new DateTime(0), 42, CancellationToken.None);

        [Benchmark]
        public ValueTask AppendInt32Event() => this.int32Track.AppendEvent(new DateTime(0), 42, CancellationToken.None);

        [Benchmark]
        public ValueTask AppendFloatEvent() => this.int32Track.AppendEvent(new DateTime(0), 42, CancellationToken.None);

        [Benchmark]
        public ValueTask AppendDoubleEvent() =>
            this.int32Track.AppendEvent(new DateTime(0), 42, CancellationToken.None);
        
        [Benchmark]
        public void AppendDoubleInMemory()
        {
            double value = 42;
            
            MemoryMarshal.Write(this.buffer.AsSpan().Slice(this.currentBufferPos, sizeof(double)), ref value);

            this.currentBufferPos = (this.currentBufferPos + sizeof(double)) % bufferSize;
        }

        [Benchmark]
        public Task AppendEventToCsvAsync() => this.csvWriter.WriteLineAsync($"{this.csvTestDate.Ticks} {this.csvTestData}");
        
        [Benchmark(Baseline = true)]
        public void AppendEventToCsv() => this.csvWriter.WriteLine($"{this.csvTestDate.Ticks} {this.csvTestData}");
    }
}

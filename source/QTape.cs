using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Qubus.Concurrency;
using Qubus.Errors;
using Qubus.Lifetime;

namespace QTapeSharp
{
    /// <summary>
    ///     A handle for an opened QTape file.
    /// </summary>
    public sealed class QTape : IAsyncDisposable
    {
        /// <summary>
        ///     Opens a QTape file.
        /// </summary>
        /// <param name="path">The path of the QTape file.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <param name="version">The targeted file format version.</param>
        /// <returns>the handle for the opened file.</returns>
        /// <exception cref="Exception">if the file could not be opened.</exception>
        public static async ValueTask<QTape> Open(string            path,
                                                  CancellationToken cancellationToken,
                                                  FormatVersion     version = FormatVersion.Latest)
        {
            var file = new RandomAccessFile(path);

            var eventSerializers = GenerateTheStandardSerializers();

            if (file.IsEmpty)
            {
                return await InitializeNewFile(file, eventSerializers, cancellationToken, version)
                           .ConfigureAwait(false);
            }

            var magicBytes = await file.ReadInt64(0, cancellationToken).ConfigureAwait(false);

            if (magicBytes != Specification.MagicBytes)
            {
                throw new Exception("Unexpected magic bytes. This file is not a valid qtape.");
            }

            var fileVersion = await file.ReadInt64(sizeof(long), cancellationToken).ConfigureAwait(false);

            var metaDataTrackOffset = await file.ReadInt64(2 * sizeof(long), cancellationToken).ConfigureAwait(false);

            var allocator     = new FileChunkAllocator(file);
            var accessManager = new ChunkAccessManager(file, allocator);

            await using AsyncOwner<RawTrack> metaDataTrack =
                await RawTrack.Open(metaDataTrackOffset, accessManager, cancellationToken).ConfigureAwait(false);

            await using RawTrackIterator metaDataIterator = metaDataTrack.Value.Iterate();

            long trackIndexMetaDataSize =
                await metaDataIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                throw new Exception("The size of the track index metadata could not be read.");
            long trackIndexType = await metaDataIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                                  throw new Exception("The type of the track index could not be read.");
            long trackIndexOffset = await metaDataIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                                    throw new Exception("The track index offset could not be read.");

            Release.Assert((MetaDataType) trackIndexType == MetaDataType.TrackIndexOffset,
                           "Expecting a different meta data item.");

            await using AsyncOwner<RawTrack> trackIndex =
                await RawTrack.Open(trackIndexOffset, accessManager, cancellationToken).ConfigureAwait(false);

            long typeInfoTrackMetaDataSize =
                await metaDataIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                throw new Exception("The size of the type info track metadata could not be read.");
            long typeInfoTrackType = await metaDataIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                                     throw new Exception("The type of the type info track could not be read.");
            long typeInfoTrackOffset = await metaDataIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                                       throw new Exception("The type info track offset could not be read.");

            await using AsyncOwner<RawTrack> typeInfoTrack =
                await RawTrack.Open(typeInfoTrackOffset, accessManager, cancellationToken).ConfigureAwait(false);

            Release.Assert((MetaDataType) typeInfoTrackType == MetaDataType.TypeInfoTrackOffset,
                           "Expecting a different meta data item.");

            await AddTypeMappingInfoToSerializerSet(typeInfoTrack.Value, eventSerializers, cancellationToken)
                    .ConfigureAwait(false);

            var tracks =
                await ReadAllTracksFromIndex(file,
                                             accessManager,
                                             trackIndex.Value,
                                             eventSerializers,
                                             cancellationToken)
                    .ConfigureAwait(false);

            return new QTape(file, accessManager, eventSerializers, trackIndex.Move(), tracks, typeInfoTrack.Move());
        }

        /// <summary>
        ///     Adds a new track to the file.
        /// </summary>
        /// <param name="id">The id of the track.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <typeparam name="T">The event type of this track.</typeparam>
        /// <returns>the handle for the new track.</returns>
        /// <exception cref="Exception">if the track could not be created.</exception>
        public async ValueTask<Track<T>> AddTrack<T>(string            id,
                                                     CancellationToken cancellationToken)
            where T : struct
        {
            using var guard = await SemaphoreGuard.Acquire(this.fileSemaphore, cancellationToken).ConfigureAwait(false);

            if (this.tracks.ContainsKey(id))
            {
                throw new Exception("The track already exists.");
            }

            var serializer = this.eventSerializers.GetSerializerForType<T>();

            await using AsyncOwner<RawTrack> dataTrack = await RawTrack.Create(this.accessManager, cancellationToken).ConfigureAwait(false);

            // TODO: Remove the type id again if it could be committed to the persistent storage.
            var (typeId, isNewTypeId) = this.eventSerializers.AcquireIdForType(typeof(T));
            
            if (isNewTypeId)
            {
                // Add the type id to the type info track.
                byte[] encodedTypeName = Specification.StoredStringEncoding.GetBytes(serializer.TypeName);

                long typeInfoEntrySize = sizeof(long) +          // The entry size itself.
                                         sizeof(long) +          // The type id.
                                         sizeof(int) +           // The length of the type id.
                                         encodedTypeName.Length; // The type name.

                await this.typeInfoTrack.Value.Write(typeInfoEntrySize, cancellationToken);
                await this.typeInfoTrack.Value.Write(typeId, cancellationToken);
                await this.typeInfoTrack.Value.Write(encodedTypeName.Length, cancellationToken);
                await this.typeInfoTrack.Value.Write(encodedTypeName, cancellationToken);
            }

            // Add the track to the track index.
            byte[] encodedId = Specification.StoredStringEncoding.GetBytes(id);

            long trackIndexEntrySize = sizeof(long) +     // The entry size itself.
                                       sizeof(int) +      // The track type.
                                       sizeof(long) +     // The start offset.
                                       sizeof(long) +     // The time index granularity.
                                       sizeof(long) +     // The time index offset.
                                       sizeof(int) +      // The length of the track id.
                                       encodedId.Length + // The track id.
                                       sizeof(long);      // The type id.

            await this.trackIndex.Value.Write(trackIndexEntrySize, cancellationToken);
            await this.trackIndex.Value.Write((int) TrackType.PrimitiveTrack, cancellationToken);
            await this.trackIndex.Value.Write(dataTrack.Value.StartOffset, cancellationToken);
            await this.trackIndex.Value.Write(-1L, cancellationToken);
            await this.trackIndex.Value.Write(-1L, cancellationToken);
            await this.trackIndex.Value.Write(encodedId.Length, cancellationToken);
            await this.trackIndex.Value.Write(encodedId, cancellationToken);
            await this.trackIndex.Value.Write(typeId, cancellationToken);

            var newTrack = new Track(id, serializer, dataTrack.Move());

            this.tracks.Add(id, newTrack);

            return new Track<T>(newTrack, serializer);
        }

        /// <summary>
        ///     Opens a existing track.
        /// </summary>
        /// <param name="id">The id of the track.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <typeparam name="T">The event type of this track.</typeparam>
        /// <returns>the handle for the track.</returns>
        /// <exception cref="Exception">if the track could not be opened.</exception>
        public async ValueTask<Track<T>> OpenTrack<T>(string id, CancellationToken cancellationToken) where T : struct
        {
            using var guard = await SemaphoreGuard.Acquire(this.fileSemaphore, cancellationToken);

            var serializer = this.eventSerializers.GetSerializerForType<T>();

            return new Track<T>(this.tracks[id], serializer);
        }

        /// <summary>
        ///     Opens a existing track as a generic track.
        /// </summary>
        /// <param name="id">The id of the track.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the handle for the track.</returns>
        /// <exception cref="Exception">if the track could not be opened.</exception>
        public async ValueTask<Track> OpenTrack(string id, CancellationToken cancellationToken)
        {
            using var guard = await SemaphoreGuard.Acquire(this.fileSemaphore, cancellationToken);

            return this.tracks[id];
        }

        /// <summary>
        ///     Opens a track or creates it, if it does not exist.
        /// </summary>
        /// <param name="id">The id of the track.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <typeparam name="T">The event type of this track.</typeparam>
        /// <returns>the handle for the track.</returns>
        /// <exception cref="Exception">if the track could not be opened.</exception>
        public async ValueTask<Track<T>> OpenOrCreateTrack<T>(string id, CancellationToken cancellationToken)
            where T : struct
        {
            IEventSerializer serializer = this.eventSerializers.GetSerializerForType<T>();

            if (this.tracks.TryGetValue(id, out Track? untypedTrack))
            {
                return new Track<T>(untypedTrack, serializer);
            }

            return await AddTrack<T>(id, cancellationToken);
        }
        
        /// <summary>
        ///     Lists all tracks.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the list of all existing tracks.</returns>
        public async IAsyncEnumerable<Track> QueryAllTracks([EnumeratorCancellation] CancellationToken cancellationToken = default)
        {
            foreach (Track track in this.tracks.Values)
            {
                yield return track;
            }
        }

        /// <summary>
        ///     Flushes all pending writes related to this file.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for this asynchronous operation.</returns>
        public async ValueTask Flush(CancellationToken cancellationToken)
        {
            // First flush all data writes.
            await this.file.Flush(cancellationToken);

            // Then write all pending meta data updates.
            await this.trackIndex.Value.UpdatePersistentMetaData(cancellationToken);
            await this.typeInfoTrack.Value.UpdatePersistentMetaData(cancellationToken);

            foreach (Track track in this.tracks.Values)
            {
                await track.UpdatePersistentMetaData(cancellationToken);
            }

            // Then flush the chunk meta data updates.
            await this.file.Flush(cancellationToken);
        }

        /// <summary>
        ///     Generates the set of standard serializers.
        /// </summary>
        /// <returns>The set of standard serializers.</returns>
        private static SerializerSet GenerateTheStandardSerializers()
        {
            var eventSerializers = new SerializerSet();

            eventSerializers.AddSerializer<int>(new Int32Serializer());
            eventSerializers.AddSerializer<long>(new Int64Serializer());
            eventSerializers.AddSerializer<float>(new FloatSerializer());
            eventSerializers.AddSerializer<double>(new DoubleSerializer());
            eventSerializers.AddSerializer<decimal>(new DecimalSerializer());

            return eventSerializers;
        }

        /// <inheritdoc />
        public async ValueTask DisposeAsync()
        {
            if (this.hasBeenDisposed == true)
            {
                return;
            }
            
            this.hasBeenDisposed = true;
            
            // Explicitly flush all updates to the persistent storage before closing the file.
            await Flush(CancellationToken.None);

            await this.typeInfoTrack.DisposeAsync();
            await this.trackIndex.DisposeAsync();

            foreach (Track track in this.tracks.Values)
            {
                await track.DisposeAsync();
            }

            this.accessManager.Dispose();

            this.fileSemaphore.Dispose();
            this.file.Dispose();
        }

        /// <summary>
        ///     Creates new file handle.
        /// </summary>
        /// <param name="file">The backing random-access file.</param>
        /// <param name="accessManager">The chunk access manager for this file.</param>
        /// <param name="eventSerializers">The set of event serializers registered for this file.</param>
        /// <param name="trackIndex">The track index of this file.</param>
        /// <param name="tracks">The list of tracks stored in this file.</param>
        /// <param name="typeInfoTrack">The type info track of this file.</param>
        private QTape(RandomAccessFile                   file,
                      ChunkAccessManager                 accessManager,
                      SerializerSet                      eventSerializers,
                      AsyncOwner<RawTrack>               trackIndex,
                      Dictionary<string, Track>          tracks,
                      AsyncOwner<RawTrack>               typeInfoTrack)
        {
            this.file             = file;
            this.accessManager    = accessManager;
            this.trackIndex       = trackIndex;
            this.tracks           = tracks;
            this.typeInfoTrack    = typeInfoTrack;
            this.eventSerializers    = eventSerializers;
        }

        /// <summary>
        ///     Initializes a new QTape file.
        /// </summary>
        /// <param name="file">The backing random-access file.</param>
        /// <param name="eventSerializers">The set of event serializers registered for this file.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <param name="version">The file format version targeted by this file.</param>
        /// <returns></returns>
        private static async ValueTask<QTape> InitializeNewFile(RandomAccessFile                   file,
                                                                SerializerSet                      eventSerializers,
                                                                CancellationToken                  cancellationToken,
                                                                FormatVersion                      version)
        {
            // Reserve the first 4096 bytes of the file for the header.
            long headerOffset = await file.Allocate(Specification.AtomicAlignment, 1, cancellationToken)
                                          .ConfigureAwait(false);

            Release.Assert(headerOffset == 0, "The header must start at the beginning of the file.");

            await file.Write(0, Specification.MagicBytes, cancellationToken).ConfigureAwait(false);

            var defaultVersion = (long) version;

            await file.Write(sizeof(long), defaultVersion, cancellationToken).ConfigureAwait(false);

            var allocator     = new FileChunkAllocator(file);
            var accessManager = new ChunkAccessManager(file, allocator);

            // Create the meta data track and write its offset into the header.
            await using AsyncOwner<RawTrack> metaDataTrack =
                await RawTrack.Create(accessManager, cancellationToken).ConfigureAwait(false);

            await file.Write(2 * sizeof(long), metaDataTrack.Value.StartOffset, cancellationToken)
                      .ConfigureAwait(false);

            await using AsyncOwner<RawTrack> trackIndex =
                await RawTrack.Create(accessManager, cancellationToken).ConfigureAwait(false);

            await using AsyncOwner<RawTrack> typeInfoTrack =
                await RawTrack.Create(accessManager, cancellationToken).ConfigureAwait(false);

            // Write the track index offset into the meta data track.
            await metaDataTrack.Value.Write((long) (3 * sizeof(long)), cancellationToken)
                               .ConfigureAwait(false); // The size of the metadata item.
            await metaDataTrack.Value.Write((long) MetaDataType.TrackIndexOffset, cancellationToken)
                               .ConfigureAwait(false);
            await metaDataTrack.Value.Write(trackIndex.Value.StartOffset, cancellationToken).ConfigureAwait(false);

            // Write the type info track offset into the meta data track.
            await metaDataTrack.Value.Write((long) (3 * sizeof(long)), cancellationToken)
                               .ConfigureAwait(false); // The size of the metadata item.
            await metaDataTrack.Value.Write((long) MetaDataType.TypeInfoTrackOffset, cancellationToken)
                               .ConfigureAwait(false);
            await metaDataTrack.Value.Write(typeInfoTrack.Value.StartOffset, cancellationToken).ConfigureAwait(false);

            // Make the entire base file structure persistent.
            await trackIndex.Value.UpdatePersistentMetaData(cancellationToken).ConfigureAwait(false);
            await typeInfoTrack.Value.UpdatePersistentMetaData(cancellationToken).ConfigureAwait(false);
            
            await metaDataTrack.Value.UpdatePersistentMetaData(cancellationToken).ConfigureAwait(false);

            await file.Flush(cancellationToken).ConfigureAwait(false);

            return new QTape(file,
                             accessManager,
                             eventSerializers,
                             trackIndex.Move(),
                             new Dictionary<string, Track>(),
                             typeInfoTrack.Move());
        }

        /// <summary>
        ///     Adds the type mapping info for this file to a serializer set set.
        /// </summary>
        /// <param name="typeInfoTrack">The type info track for this file.</param>
        /// <param name="eventSerializers">The serializer set to which the info should be added.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for this asynchronous operation.</returns>
        /// <exception cref="Exception">if the type mapping is invalid.</exception>
        private static async ValueTask AddTypeMappingInfoToSerializerSet(RawTrack typeInfoTrack,
                                                                       SerializerSet eventSerializers,
                                                                       CancellationToken cancellationToken)
        {
            RawTrackIterator typeInfoTrackIterator = typeInfoTrack.Iterate();

            for (;;)
            {
                long? typeInfoEntrySize =
                    await typeInfoTrackIterator.TryReadInt64(cancellationToken).ConfigureAwait(false);

                if (typeInfoEntrySize == null)
                {
                    return;
                }

                long typeId = await typeInfoTrackIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                              throw new Exception("Unable to read the type id.");
                int typeNameLength =
                    await typeInfoTrackIterator.TryReadInt32(cancellationToken).ConfigureAwait(false) ??
                    throw new Exception("Unable to read the length of the type name.");

                var typeNameBuffer = new Memory<byte>(new byte[typeNameLength]);

                if (!await typeInfoTrackIterator.TryRead(typeNameBuffer, typeNameLength, cancellationToken)
                                                .ConfigureAwait(false))
                {
                    throw new Exception("Unable to read the type name.");
                }

                string typeName = Specification.StoredStringEncoding.GetString(typeNameBuffer.Span);

                IEventSerializer serializer = eventSerializers.GetSerializerForTypeName(typeName);

                eventSerializers.MapTypeToExistingId(serializer.EventType, typeId);
            }
        }

        /// <summary>
        ///     Reads the list of tracks from the index.
        /// </summary>
        /// <param name="file">The backing random-access file.</param>
        /// <param name="accessManager">The chunk access manager for this file.</param>
        /// <param name="trackIndex">The track index for this file.</param>
        /// <param name="eventSerializers">The set of serializers registered for this file.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>The table of all existing tracks.</returns>
        /// <exception cref="Exception">if the track index is invalid.</exception>
        private static async ValueTask<Dictionary<string, Track>> ReadAllTracksFromIndex(
            RandomAccessFile                   file,
            ChunkAccessManager                 accessManager,
            RawTrack                           trackIndex,
            SerializerSet                      eventSerializers,
            CancellationToken                  cancellationToken)
        {
            RawTrackIterator trackIndexIterator = trackIndex.Iterate();

            var tracks = new Dictionary<string, Track>();

            try
            {
                for (;;)
                {
                    long? trackIndexEntrySize =
                        await trackIndexIterator.TryReadInt64(cancellationToken).ConfigureAwait(false);

                    if (trackIndexEntrySize == null)
                    {
                        return tracks;
                    }

                    var trackType =
                        (TrackType) (await trackIndexIterator.TryReadInt32(cancellationToken).ConfigureAwait(false) ??
                                     throw new Exception("The track type could not be read."));
                    long startOffset = await trackIndexIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                                       throw new Exception("The start offset of the track could not be read.");
                    long timeIndexGranularity =
                        await trackIndexIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                        throw new Exception(
                            "The time index granularity of the track could not be read.");
                    long timeIndexOffset =
                        await trackIndexIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                        throw new Exception("The time index offset of the track could not be read.");
                    int idLength = await trackIndexIterator.TryReadInt32(cancellationToken).ConfigureAwait(false) ??
                                   throw new Exception("The length of the track id could not be read.");

                    var idBuffer = new Memory<byte>(new byte[idLength]);

                    if (!await trackIndexIterator.TryRead(idBuffer, idLength, cancellationToken).ConfigureAwait(false))
                    {
                        throw new Exception("The track id could not be read.");
                    }

                    string id = Specification.StoredStringEncoding.GetString(idBuffer.Span);

                    long typeId = await trackIndexIterator.TryReadInt64(cancellationToken).ConfigureAwait(false) ??
                                  throw new Exception("The type id of the track could not be read.");

                    IEventSerializer serializer = eventSerializers.GetSerializerForTypeId(typeId);

                    var rawTrack = await RawTrack.Open(startOffset, accessManager, cancellationToken)
                                                 .ConfigureAwait(false);

                    var track = new Track(id, serializer, rawTrack.Move());

                    tracks.Add(id, track);
                }
            }
            catch
            {
                foreach (Track track in tracks.Values)
                {
                    await track.DisposeAsync();
                }

                throw;
            }
        }

        /// <summary>
        ///     Specifies if the file handle has already been disposed.
        /// </summary>
        private bool hasBeenDisposed = false;

        /// <summary>
        ///     The binary semaphore protecting the handle against concurrent accesses.
        /// </summary>
        private readonly SemaphoreSlim fileSemaphore = new SemaphoreSlim(1, 1);

        /// <summary>
        ///     The lock used to ensure memory consistency for the internal data structures of the handle.
        /// </summary>
        private readonly object padlock = new object();

        /// <summary>
        ///     The backing random-access file.
        /// </summary>
        private readonly RandomAccessFile file;

        /// <summary>
        ///     The chunk access manager for this file.
        /// </summary>
        private readonly ChunkAccessManager accessManager;

        /// <summary>
        ///     The track index for this file.
        /// </summary>
        private readonly AsyncOwner<RawTrack> trackIndex;

        /// <summary>
        ///     The type info track for this file.
        /// </summary>
        private readonly AsyncOwner<RawTrack> typeInfoTrack;

        /// <summary>
        ///     The table of tracks stored in this file.
        /// </summary>
        private readonly Dictionary<string, Track> tracks = new Dictionary<string, Track>();

        /// <summary>
        ///     The set of event serializers registered for this file.
        /// </summary>
        private readonly SerializerSet eventSerializers;
    }
}

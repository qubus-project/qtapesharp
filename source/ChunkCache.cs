using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace QTapeSharp
{
    /// <summary>
    ///     A per-file in-memory chunk cache.
    /// </summary>
    public sealed class ChunkCache
    {
        /// <summary>
        ///     Caches a specific file chunk.
        /// </summary>
        /// <param name="chunk">The file chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>The in-memory chunk carrying the cached data.</returns>
        /// <exception cref="Exception">if the chunk could not be cached, including previous attempts to do this.</exception>
        public async ValueTask<InMemoryChunk> CacheChunk(FileChunk chunk, CancellationToken cancellationToken)
        {
            CacheEntry entry = await this.cachedChunks.GetOrAdd(chunk.StartOffset,
                                                                (offset, newChunk) => loadChunkIntoCacheEntry(newChunk),
                                                                chunk);

            return entry.CachedData;
        }

        /// <summary>
        ///     Flushes any pending updates made to a cached chunk.
        /// </summary>
        /// <remarks>
        ///     This operation does *not* flush the updates to the persistent storage but only
        ///     forwards them to the file-layer.
        /// </remarks>
        /// <param name="offset">The offset of the chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>A handle for the asynchronous operation.</returns>
        /// <exception cref="Exception">if the updates could not be flushed.</exception>
        public async ValueTask FlushChunk(long offset, CancellationToken cancellationToken)
        {
            if (!this.cachedChunks.TryGetValue(offset, out Task<CacheEntry>? entry))
            {
                throw new Exception(
                    $"The specified chunk (offset {offset}) is not managed by this cache.");
            }

            await flushChunkInternal(entry, cancellationToken);
        }

        /// <summary>
        ///     Evicts the specified chunk from the cache.
        /// </summary>
        /// <param name="chunkOffset">The offset of the chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>true if the chunk was cached and has been evicted, false otherwise.</returns>
        public async ValueTask<bool> EvictChunk(long chunkOffset, CancellationToken cancellationToken)
        {
            if (!this.cachedChunks.TryRemove(chunkOffset, out Task<CacheEntry>? entry))
            {
                return false;
            }

            // Flush the content of the cached chunk before evicting the data from the cache.
            await flushChunkInternal(entry, cancellationToken);
            
            return true;
        }

        /// <summary>
        ///     Loads the specific chunk into a cache entry.
        /// </summary>
        /// <param name="chunk">The chunk.</param>
        /// <returns>The new cache entry.</returns>
        /// <exception cref="Exception">if not all chunk data could be read into the cache.</exception>
        private static async Task<CacheEntry> loadChunkIntoCacheEntry(FileChunk chunk)
        {
            var data = new byte[chunk.ChunkSize];

            int chunkRead = await chunk.ReadRaw(0,
                                                data.AsMemory().Slice(0, chunk.UsedBytes),
                                                CancellationToken.None);

            if (chunkRead < chunk.UsedBytes)
            {
                throw new Exception($"Only {chunkRead} of {chunk.UsedBytes} have been read into the cache.");
            }

            return new CacheEntry(chunk, new InMemoryChunk(data));
        }

        /// <summary>
        ///     Flushes the specified cache entry.
        /// </summary>
        /// <param name="entry">The cache entry.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>A handle for the asynchronous operation.</returns>
        private static async ValueTask flushChunkInternal(Task<CacheEntry> entry, CancellationToken cancellationToken)
        {
            CacheEntry loadedEntry = await entry;

            FileChunk chunk = loadedEntry.CachedChunk;
            
            await loadedEntry.CachedData.FlushDataInto(chunk, cancellationToken);

            // Only flush the data. This is necessary since otherwise portions of the data might be written
            // after the metadata update, making the persistent storage inconsistent.
            await chunk.FlushData(cancellationToken);
            
            await loadedEntry.CachedData.FlushMetadataInto(chunk, cancellationToken);
        }

        /// <summary>
        ///     A cache entry.
        /// </summary>
        private sealed class CacheEntry
        {
            /// <summary>
            ///     Creates a new cache entry.
            /// </summary>
            /// <param name="cachedChunk">The cached file chunk.</param>
            /// <param name="cachedData">The cached data.</param>
            public CacheEntry(FileChunk cachedChunk, InMemoryChunk cachedData)
            {
                this.CachedChunk = cachedChunk;
                this.CachedData  = cachedData;
            }

            /// <summary>
            ///     The cached file chunk.
            /// </summary>
            public readonly FileChunk     CachedChunk;
            
            /// <summary>
            ///     The cached data.
            /// </summary>
            public readonly InMemoryChunk CachedData;
        }

        /// <summary>
        ///     The table of all cached chunks.
        /// </summary>
        private readonly ConcurrentDictionary<long, Task<CacheEntry>> cachedChunks =
            new ConcurrentDictionary<long, Task<CacheEntry>>();
    }
}

using System;
using System.Text;

namespace QTapeSharp
{
    /// <summary>
    ///     The currently available format versions.
    /// </summary>
    public enum FormatVersion : long
    {
        V1     = 100000000,
        Latest = -1
    }

    /// <summary>
    ///     The available meta data types.
    /// </summary>
    internal enum MetaDataType : long
    {
        TrackIndexOffset    = 1,
        TypeInfoTrackOffset = 2
    }

    /// <summary>
    ///     A set of constants and helper functions relating to the file format specification.
    /// </summary>
    internal static class Specification
    {
        /// <summary>
        ///     The magic bytes marking a QTape file.
        /// </summary>
        public const long MagicBytes = 0x45504154455543;

        /// <summary>
        ///     The magic bytes marking a file chunk.
        /// </summary>
        public const long ChunkMagicBytes = 0x6b6e756863657563;

        /// <summary>
        ///     The magic bytes marking a raw track.
        /// </summary>
        public const long RawTrackMagicBytes = 0x6b63617274776172;

        // To ensure that a write to the disc is atomic, it has to be aligned
        // to a physical sector boundary. Since the physical sector size is either 512 byte
        // or 4096 byte in practise, we align atomic writes to a 4096 byte boundary which covers both cases.
        // 
        // In principle, it would be better to query the actual physical sector size from the device.
        // Since it is non-trivial to get the physical sector size in a portable way without elevated
        // privileges, we use this pragmatic value. As a bonus, the writes will stay atomic if the file is
        // relocated to a derive with a larger physical sector size.
        public const short AtomicAlignment = 4096;

        // To ensure that a write to the disc is atomic, it has to be smaller than the
        // extent of a physical sector. Since the physical sector size is either 512 byte
        // or 4096 byte in practise, we choose the smallest value of 512 byte to cover both cases.
        // 
        // In principle, it would be better to query the actual physical sector size from the device.
        // Since it is non-trivial to get the physical sector size in a portable way without elevated
        // privileges, we use this pragmatic value. As a bonus, the writes will stay atomic if the file is
        // relocated to a derive with a smaller physical sector size.
        public const short AtomicSize = 512;

        /// <summary>
        ///     The encoding used to store strings within a QTape file.
        /// </summary>
        public static readonly Encoding StoredStringEncoding = Encoding.UTF8;

        /// <summary>
        ///     Converts a time point to the internal time format of a QTape file.
        /// </summary>
        /// <param name="date">The time point.</param>
        /// <returns>the number of ticks representing this time point.</returns>
        public static long ToTicks(DateTime date)
        {
            // A tick within the QTape file format is defined as 100 ns which corresponds to
            // a DateTime tick.
            return date.Ticks;
        }

        /// <summary>
        ///     Converts a time span to the internal time format of a QTape file.
        /// </summary>
        /// <param name="duration">The time span.</param>
        /// <returns>the number of ticks representing this time span.</returns>
        public static long ToTicks(TimeSpan duration)
        {
            // A tick within the QTape file format is defined as 100 ns which corresponds to
            // a TimeSpan tick.
            return duration.Ticks;
        }

        /// <summary>
        ///     Converts the internal time format of a QTape file to a time point.
        /// </summary>
        /// <param name="ticks">The number of ticks representing the time point.</param>
        /// <returns>the time point.</returns>
        public static DateTime ToDateTime(long ticks)
        {
            return new DateTime(ticks);
        }

        /// <summary>
        ///     A set of constants and helper functions relating to the chunk format.
        /// </summary>
        public static class Chunk
        {
            /// <summary>
            ///     The size of the chunk header in bytes.
            /// </summary>
            public const int HeaderSize = sizeof(long) + // The chunk magic bytes
                                           sizeof(int) +  // The chunk size
                                           sizeof(int) +  // The number of used bytes.
                                           sizeof(long);  // The offset to the next chunk or -1 if this is a tail chunk.

            /// <summary>
            ///     The size of the 'chunk size' field.
            /// </summary>
            public const int ChunkSizeSize = sizeof(int);
            
            /// <summary>
            ///     The size of the 'used bytes' field.
            /// </summary>
            public const int UsedBytesSize = sizeof(int);
            
            /// <summary>
            ///     The size of the 'next chunk offset' field.
            /// </summary>
            public const int NextChunkOffsetSize = sizeof(long);
            
            /// <summary>
            ///     The chunk-relative offset for the chunk id.
            /// </summary>
            public const int RelativeIdOffset = 0;
            
            /// <summary>
            ///     The chunk-relative offset for the chunk size.
            /// </summary>
            public const int RelativeSizeOffset = RelativeIdOffset + sizeof(long);

            /// <summary>
            ///     The chunk-relative offset for the 'used bytes' field.
            /// </summary>
            public const int RelativeUsedBytesOffset = RelativeSizeOffset + ChunkSizeSize;

            /// <summary>
            ///     The chunk-relative offset for the next chunk offset.
            /// </summary>
            public const int RelativeNextChunkOffsetOffset = RelativeUsedBytesOffset + UsedBytesSize;

            /// <summary>
            ///     The chunk-relative offset for data portion of the chunk.
            /// </summary>
            public const int RelativeDataOffset = RelativeNextChunkOffsetOffset + NextChunkOffsetSize;
            
            /// <summary>
            ///     Calculates the absolute file offset for the 'id' field of this chunk.
            /// </summary>
            /// <param name="startOffset">The start offset of this chunk.</param>
            /// <returns>the file offset for the id field.</returns>
            public static long CalculateIdOffset(long startOffset)
            {
                return startOffset + RelativeIdOffset;
            }
            
            /// <summary>
            ///     Calculates the absolute file offset for the 'chunk size' field of this chunk.
            /// </summary>
            /// <param name="startOffset">The start offset of this chunk.</param>
            /// <returns>the file offset for the 'chunk size' field.</returns>
            public static long CalculateSizeOffset(long startOffset)
            {
                return startOffset + RelativeSizeOffset;
            }

            /// <summary>
            ///     Calculates the absolute file offset for the 'used bytes' field of this chunk.
            /// </summary>
            /// <param name="startOffset">The start offset of this chunk.</param>
            /// <returns>the file offset for the 'used bytes' field.</returns>
            public static long CalculateUsedBytesOffset(long startOffset)
            {
                return startOffset + RelativeUsedBytesOffset;
            }

            /// <summary>
            ///     Calculates the absolute file offset for the 'next chunk offset' field of this chunk.
            /// </summary>
            /// <param name="startOffset">The start offset of this chunk.</param>
            /// <returns>the file offset for the 'next chunk offset' field.</returns>
            public static long CalculateNextChunkOffsetOffset(long startOffset)
            {
                return startOffset + RelativeNextChunkOffsetOffset;
            }

            /// <summary>
            ///     Calculates the absolute file offset marking the start of the data portion of this chunk.
            /// </summary>
            /// <param name="startOffset">The start offset of this chunk.</param>
            /// <returns>the file offset marking the start of the data portion of this chunk.</returns>
            public static long CalculateDataOffset(long startOffset)
            {
                return startOffset + RelativeDataOffset;
            }
        }
    }
}

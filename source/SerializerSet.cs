﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QTapeSharp
{
    /// <summary>
    ///     A set of event serializers.
    /// </summary>
    public class SerializerSet
    {
        /// <summary>
        ///     Adds a serializer for a specific type.
        /// </summary>
        /// <param name="serializer">The serializer.</param>
        /// <typeparam name="T">The event type for which the serializer should be registered.</typeparam>
        /// <exception cref="Exception">if a serializer could not be registered.</exception>
        public void AddSerializer<T>(IEventSerializer serializer)
        {
            try
            {
                this.eventSerializers.Add(typeof(T), serializer);
            }
            catch (ArgumentException e)
            {
                throw new Exception($"A serializer has already been registered for type {typeof(T).FullName}.", e);
            }
        }

        /// <summary>
        ///     Acquires the id for the specified event type.
        /// </summary>
        /// <param name="type">The event type.</param>
        /// <returns>the type id corresponding to the specified event type plus a flag specifying if the id has been newly generated.</returns>
        public (long typeId, bool isNew) AcquireIdForType(Type type)
        {
            if (!this.typeIdTable.TryGetValue(type, out long typeId))
            {
                typeId = this.typeIdTable.Count;
                this.typeIdTable.Add(type, typeId);
                
                return (typeId, true);
            }

            return (typeId, false);
        }

        /// <summary>
        ///     Adds an existing event type to type id mapping.
        /// </summary>
        /// <param name="datatype">The event type.</param>
        /// <param name="typeId">The type id.</param>
        /// <exception cref="Exception">if a mapping for the type already exists.</exception>
        public void MapTypeToExistingId(Type datatype, long typeId)
        {
            try
            {
                this.typeIdTable.Add(datatype, typeId);
            }
            catch (ArgumentException e)
            {
                throw new Exception($"A mapping for the type '{datatype.FullName}' and the type id '{typeId}' already exists.", e);
            }
        }
        
        /// <summary>
        ///     Serializes an event.
        /// </summary>
        /// <param name="payload">The event payload.</param>
        /// <param name="buffer">The buffer used to store the serialized representation of the event.</param>
        /// <exception cref="Exception">if the event could not be serialized.</exception>
        public void Serialize(object payload, Span<byte> buffer)
        {
            Type datatype = payload.GetType();
            
            if (!this.eventSerializers.TryGetValue(datatype, out IEventSerializer? serializer))
            {
                throw new Exception($"No serializer has been registered for the type '{datatype.FullName}'");
            }
            
            serializer.Serialize(payload, buffer);
        }
        
        /// <summary>
        ///     Deserializes an event.
        /// </summary>
        /// <param name="buffer">The buffer containing the serialized representation of the event.</param>
        /// <returns>the deserialized event.</returns>
        /// <exception cref="Exception">if the event could not be deserialized.</exception>
        public object Deserialize<T>(Span<byte> buffer)
        {
            Type datatype = typeof(T);
            
            if (!this.eventSerializers.TryGetValue(datatype, out IEventSerializer? serializer))
            {
                throw new Exception($"No serializer has been registered for the type '{datatype.FullName}'");
            }
            
            return serializer.Deserialize(buffer);
        }

        /// <summary>
        ///     Acquires the specific serializer for a certain event type.
        /// </summary>
        /// <typeparam name="T">The event type.</typeparam>
        /// <returns>the serializer for this event type.</returns>
        /// <exception cref="Exception">if no serializer for this event type is available.</exception>
        public IEventSerializer GetSerializerForType<T>()
        {
            return GetSerializerForType(typeof(T));
        }
        
        /// <summary>
        ///     Acquires the specific serializer for a certain event type.
        /// </summary>
        /// <params name="type">The event type.</params>
        /// <returns>the serializer for this event type.</returns>
        /// <exception cref="Exception">if no serializer for this event type is available.</exception>
        public IEventSerializer GetSerializerForType(Type type)
        {
            if (!this.eventSerializers.TryGetValue(type, out IEventSerializer? serializer))
            {
                throw new Exception($"No serializer has been registered for the type '{type.FullName}'");
            }

            return serializer;
        }

        /// <summary>
        ///     Acquires the specific serializer for a certain event type via its type id.
        /// </summary>
        /// <params name="typeId">The type id of the event.</params>
        /// <returns>the serializer for this event type.</returns>
        /// <exception cref="Exception">if no serializer for this event type is available.</exception>
        public IEventSerializer GetSerializerForTypeId(long typeId)
        {
            KeyValuePair<Type, long>? selectedEntry = this.typeIdTable.FirstOrDefault(entry => entry.Value == typeId);

            if (selectedEntry == null)
            {
                throw new Exception($"No type has been registered for the type id '{typeId}'");
            }

            return GetSerializerForType(selectedEntry.Value.Key);
        }
        
        /// <summary>
        ///     Acquires the specific serializer for a certain event type via its type name.
        /// </summary>
        /// <params name="typeName">The type name of the event.</params>
        /// <returns>the serializer for this event type.</returns>
        /// <exception cref="Exception">if no serializer for this event type is available.</exception>
        public IEventSerializer GetSerializerForTypeName(string typeName)
        {
            KeyValuePair<Type, IEventSerializer>? selectedEntry = this.eventSerializers.FirstOrDefault(entry => entry.Value.TypeName == typeName);

            if (selectedEntry == null)
            {
                throw new Exception($"No serializer has been registered for the type name '{typeName}'");
            }

            return selectedEntry.Value.Value;
        }
        
        private readonly Dictionary<Type, IEventSerializer> eventSerializers = new Dictionary<Type, IEventSerializer>();

        private readonly Dictionary<Type, long> typeIdTable = new Dictionary<Type, long>();
    }
}

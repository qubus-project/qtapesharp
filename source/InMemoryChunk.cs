using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace QTapeSharp
{
    /// <summary>
    ///     An in-memory version of a file chunk.
    /// </summary>
    public sealed class InMemoryChunk
    {
        /// <summary>
        ///     Creates a new in-memory chunk from existing chunk data.
        /// </summary>
        /// <param name="data"></param>
        public InMemoryChunk(byte[] data)
        {
            this.data = data;
        }

        /// <summary>
        ///     The total size of the chunk in bytes.
        /// </summary>
        public int ChunkSize
        {
            get
            {
                int offset = Specification.Chunk.RelativeSizeOffset;

                return MemoryMarshal.Read<int>(this.data.AsSpan().Slice(offset, Specification.Chunk.ChunkSizeSize));
            }
            
            private set
            {
                int offset = Specification.Chunk.RelativeSizeOffset;

                MemoryMarshal.Write(this.data.AsSpan().Slice(offset, Specification.Chunk.ChunkSizeSize), ref value);
            }
        }

        /// <summary>
        ///     The number of bytes which are already in use.
        /// </summary>
        /// <remarks>
        ///     This does include the chunk header.
        /// </remarks>
        public int UsedBytes
        {
            get
            {
                int offset = Specification.Chunk.RelativeUsedBytesOffset;

                return MemoryMarshal.Read<int>(this.data.AsSpan().Slice(offset, Specification.Chunk.UsedBytesSize));
            }
            
            private set
            {
                int offset = Specification.Chunk.RelativeUsedBytesOffset;

                MemoryMarshal.Write(this.data.AsSpan().Slice(offset, Specification.Chunk.UsedBytesSize), ref value);
            }
        }

        /// <summary>
        ///     Queries the offset of the next chunk, if any.
        /// </summary>
        /// <returns>the offset of the next chunk, if any.</returns>
        public long? QueryNextChunkOffset()
        {
            int offset = Specification.Chunk.RelativeNextChunkOffsetOffset;
            
            long nextChunkOffset = MemoryMarshal.Read<long>(this.data.AsSpan().Slice(offset, Specification.Chunk.NextChunkOffsetSize));
            
            if (nextChunkOffset == -1L)
            {
                return null;
            }

            return nextChunkOffset;
        }
        
        /// <summary>
        ///     Reads some bytes from this chunk.
        /// </summary>
        /// <remarks>
        ///     This routine will read at most the number of bytes which fit into the provided buffer.
        ///     Less bytes than this maximum might be read by a single call. The actual number is returned
        ///     by the routine.
        /// </remarks>
        /// <param name="offset">The relative start offset for the read.</param>
        /// <param name="buffer">The buffer into which the data is read.</param>
        /// <returns>the number of actually read bytes.</returns>
        public int Read(int offset, Span<byte> buffer)
        {
            int actualDataSize = this.UsedBytes - Specification.Chunk.HeaderSize;
            int availableData  = actualDataSize - offset;

            int readableSize = Math.Min(availableData, buffer.Length);

            this.data.AsSpan().Slice(Specification.Chunk.HeaderSize + offset, readableSize).CopyTo(buffer);

            return readableSize;
        }

        /// <summary>
        ///     Writes new data to this chunk.
        /// </summary>
        /// <remarks>
        ///     This routine will write at most the number of bytes which are provided by the buffer.
        ///     Less bytes than this maximum might be written by a single call. The actual number is returned
        ///     by the routine.
        /// </remarks>
        /// <param name="buffer">The buffer from which the data is written to the chunk.</param>
        /// <returns>the number of actually written bytes.</returns>
        public int Write(ReadOnlySpan<byte> buffer)
        {
            int availableStorage = this.ChunkSize - this.UsedBytes;

            int writableSize = Math.Min(availableStorage, buffer.Length);

            buffer.CopyTo(new Span<byte>(this.data, this.UsedBytes, writableSize));

            this.UsedBytes += writableSize;

            return writableSize;
        }

        /// <summary>
        ///     Writes some data *atomically* to this chunk.
        /// </summary>
        /// <remarks>
        ///     This function is meant to be used if a write needs to be
        ///     atomic to ensure consistency.
        /// 
        ///     The atomicity guarantee means that a call with either write all
        ///     data to this chunk or none at all. If the data does not fit into
        ///     this chunk, a new chunk has to be allocated. If the amount of data is
        ///     larger than the overall size of a chunk, it is impossible to write
        ///     the data atomically at all since even an empty chunk could not contain it.
        ///     Calling code is responsible for taking this into account.
        /// </remarks>
        /// <param name="buffer">The buffer from which the data is written to the chunk.</param>
        /// <returns>true if the data has been written, false otherwise.</returns>
        public bool TryWriteAtomically(ReadOnlySpan<byte> buffer)
        {
            int availableStorage = this.ChunkSize - this.UsedBytes;

            if (!buffer.TryCopyTo(new Span<byte>(this.data, this.UsedBytes, availableStorage)))
            {
                return false;
            }
            
            this.UsedBytes += buffer.Length;

            return true;
        }

        /// <summary>
        ///     Finalizes this chunk.
        /// </summary>
        /// <remarks>
        ///     After a chunk has been finalized, is must not be modified anymore. Any new data
        ///     should be written to the next chunk.
        /// </remarks>
        /// <param name="nextChunkOffset">The absolute file offset of the following chunk.</param>
        /// <returns>the handle for this asynchronous operation.</returns>
        public void Finalize(long nextChunkOffset)
        {
            int offset = Specification.Chunk.RelativeNextChunkOffsetOffset;

            MemoryMarshal.Write(this.data.AsSpan().Slice(offset, Specification.Chunk.NextChunkOffsetSize), ref nextChunkOffset);
        }

        /// <summary>
        ///     Flushes the data portion into a <see cref="FileChunk"/>.
        /// </summary>
        /// <remarks>
        ///     This function does not ensure that all data is actually written to the persistent storage
        ///     but only starts the process of commiting any pending writes from the cache into the persistent storage.
        ///     To ensure that all writes are actually persistent, a file-level flush is still necessary.
        /// </remarks>
        /// <param name="targetChunk">The target chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>A handle for the asynchronous operation.</returns>
        public async ValueTask FlushDataInto(FileChunk targetChunk, CancellationToken cancellationToken)
        {
            await targetChunk.WriteRaw(Specification.Chunk.RelativeDataOffset, this.data.AsMemory().Slice(Specification.Chunk.RelativeDataOffset), cancellationToken);
        }
        
        /// <summary>
        ///     Flushes the metadata portion into a <see cref="FileChunk"/>.
        /// </summary>
        /// <remarks>
        ///     This function does not ensure that the metadata is actually written to the persistent storage
        ///     but only starts the process of commiting any pending metadata updates from the cache into the persistent storage.
        ///     To ensure that all updates are actually persistent, a file-level flush is still necessary.
        /// </remarks>
        /// <param name="targetChunk">The target chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>A handle for the asynchronous operation.</returns>
        public async ValueTask FlushMetadataInto(FileChunk targetChunk, CancellationToken cancellationToken)
        {
            await targetChunk.WriteRaw(0, this.data.AsMemory().Slice(0, Specification.Chunk.RelativeDataOffset), cancellationToken);
        }
        
        /// <summary>
        ///     The cached data of the chunk.
        /// </summary>
        private readonly byte[] data;
    }
}

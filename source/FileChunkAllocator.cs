using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using Qubus.Concurrency;
using Qubus.Errors;

namespace QTapeSharp
{
    /// <summary>
    ///     A chunk of a file which has been allocated for data storage.
    /// </summary>
    public sealed class FileChunk
    {
        /// <summary>
        ///     Creates a new file chunk.
        /// </summary>
        /// <param name="file">The backing file of this chunk.</param>
        /// <param name="startOffset">The start offset of this chunk.</param>
        /// <param name="chunkSize">The total size of this chunk, including the metadata.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the new file chunk.</returns>
        public static async ValueTask<FileChunk> Create(RandomAccessFile  file,
                                                        long              startOffset,
                                                        int               chunkSize,
                                                        CancellationToken cancellationToken)
        {
            Debug.Assert(Specification.Chunk.HeaderSize <= Specification.AtomicSize);

            Release.Assert(chunkSize >= Specification.Chunk.HeaderSize,
                           "The chunk is too small to incorporate the header.");

            Release.Assert(chunkSize > 0, "The chunk size must be larger than zero.");

            // Write the chunk header.
            await file.Write(Specification.Chunk.CalculateIdOffset(startOffset), Specification.ChunkMagicBytes, cancellationToken).ConfigureAwait(false);
            await file.Write(Specification.Chunk.CalculateSizeOffset(startOffset), chunkSize, cancellationToken).ConfigureAwait(false);
            await file.Write(Specification.Chunk.CalculateUsedBytesOffset(startOffset), Specification.Chunk.HeaderSize, cancellationToken).ConfigureAwait(false);
            await file.Write(Specification.Chunk.CalculateNextChunkOffsetOffset(startOffset), -1L, cancellationToken).ConfigureAwait(false);

            return new FileChunk(file, startOffset, chunkSize, Specification.Chunk.HeaderSize);
        }

        /// <summary>
        ///     Opens an existing chunk.
        /// </summary>
        /// <param name="file">The backing file of this chunk.</param>
        /// <param name="startOffset">The start offset of the existing chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the exiting chunk.</returns>
        /// <exception cref="Exception">if no valid chunk does exist at this position.</exception>
        public static async ValueTask<FileChunk> Open(RandomAccessFile  file,
                                                      long              startOffset,
                                                      CancellationToken cancellationToken)
        {
            // Read the chunk header.
            var chunkMagicBytes = await file.ReadInt64(Specification.Chunk.CalculateIdOffset(startOffset), cancellationToken).ConfigureAwait(false);

            if (chunkMagicBytes != Specification.ChunkMagicBytes)
            {
                throw new Exception("An invalid chunk has been encountered.");
            }
            
            var chunkSize = await file.ReadInt32(Specification.Chunk.CalculateSizeOffset(startOffset), cancellationToken).ConfigureAwait(false);
            var usedBytes = await file.ReadInt32(Specification.Chunk.CalculateUsedBytesOffset(startOffset), cancellationToken).ConfigureAwait(false);

            return new FileChunk(file, startOffset, chunkSize, usedBytes);
        }

        /// <summary>
        ///     Creates a new file chunk object.
        /// </summary>
        /// <remarks>
        ///     This constructor does <em>not</em> create an actual chunk, just the in-memory wrapper.
        /// </remarks>
        /// <param name="file">The backing file of this chunk.</param>
        /// <param name="startOffset">The start offset of this chunk.</param>
        /// <param name="chunkSize">The total size of this chunk, including the metadata.</param>
        /// <param name="usedBytes">The number of already used bytes, including metadata.</param>
        private FileChunk(RandomAccessFile file, long startOffset, int chunkSize, int usedBytes)
        {
            this.file        = file;
            this.StartOffset = startOffset;
            this.EndOffset   = startOffset + chunkSize;

            this.ChunkSize = chunkSize;

            this.UsedBytes = usedBytes;
        }

        /// <summary>
        ///     The start offset of this chunk.
        /// </summary>
        public long StartOffset { get; }
        
        /// <summary>
        ///     The end offset of this chunk.
        /// </summary>
        public long EndOffset   { get; }

        /// <summary>
        ///     The total size of the chunk in bytes.
        /// </summary>
        public int ChunkSize { get; }

        /// <summary>
        ///     The number of bytes which are already in use.
        /// </summary>
        /// <remarks>
        ///     This does include the chunk header.
        /// </remarks>
        public int UsedBytes { get; private set; }

        /// <summary>
        ///     The absolute offset of the 'used bytes' field.
        /// </summary>
        private long UsedBytesOffset => Specification.Chunk.CalculateUsedBytesOffset(this.StartOffset);

        /// <summary>
        ///     The absolute offset of the 'next chunk offset' field.
        /// </summary>
        private long NextChunkOffsetOffset => Specification.Chunk.CalculateNextChunkOffsetOffset(this.StartOffset);

        /// <summary>
        ///     The absolute offset of the payload portion of this chunk.
        /// </summary>
        private long DataOffset => Specification.Chunk.CalculateDataOffset(this.StartOffset);

        /// <summary>
        ///     Calculates an absolute file offset from a relative offset for this chunk.
        /// </summary>
        /// <param name="relativeOffset">The relative offset with respect to this chunk.</param>
        /// <returns>the absolute file offset.</returns>
        public long CalculateAbsoluteOffset(long relativeOffset)
        {
            return this.StartOffset + relativeOffset;
        }

        /// <summary>
        ///     Queries the offset of the next chunk, if any.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the offset of the next chunk, if any.</returns>
        public async ValueTask<long?> QueryNextChunkOffset(CancellationToken cancellationToken)
        {
            var buffer = new Memory<byte>(new byte[sizeof(long)]);
            
            await this.file.Read(this.NextChunkOffsetOffset, buffer, cancellationToken).ConfigureAwait(false);

            long nextChunkOffset = MemoryMarshal.Read<long>(buffer.Span);

            if (nextChunkOffset == -1L)
            {
                return null;
            }

            return nextChunkOffset;
        }

        /// <summary>
        ///     Writes new data to this chunk.
        /// </summary>
        /// <remarks>
        ///     This routine will write at most the number of bytes which are provided by the buffer.
        ///     Less bytes than this maximum might be written by a single call. The actual number is returned
        ///     by the routine.
        /// </remarks>
        /// <param name="data">The buffer from which the data is written to the chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the number of actually written bytes.</returns>
        public async ValueTask<int> Write(ReadOnlyMemory<byte> data, CancellationToken cancellationToken)
        {
            int availableStorage = this.ChunkSize - this.UsedBytes;

            int writableSize = Math.Min(availableStorage, data.Length);
            
            await this.file.Write(this.StartOffset + this.UsedBytes, data.Slice(0, writableSize), cancellationToken).ConfigureAwait(false);

            this.UsedBytes += writableSize;

            return writableSize;
        }

        /// <summary>
        ///     Writes some data *atomically* to this chunk.
        /// </summary>
        /// <remarks>
        ///     This function is meant to be used if a write needs to be
        ///     atomic to ensure consistency.
        /// 
        ///     The atomicity guarantee means that a call with either write all
        ///     data to this chunk or none at all. If the data does not fit into
        ///     this chunk, a new chunk has to be allocated. If the amount of data is
        ///     larger than the overall size of a chunk, it is impossible to write
        ///     the data atomically at all since even an empty chunk could not contain it.
        ///     Calling code is responsible for taking this into account.
        /// </remarks>
        /// <param name="data">The buffer from which the data is written to the chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>true if the data has been written, false otherwise.</returns>
        public async ValueTask<bool> TryWriteAtomically(ReadOnlyMemory<byte> data, CancellationToken cancellationToken)
        {
            int availableStorage = this.ChunkSize - this.UsedBytes;

            if (availableStorage < data.Length)
            {
                return false;
            }
            
            await this.file.Write(this.StartOffset + this.UsedBytes, data, cancellationToken).ConfigureAwait(false);
            
            this.UsedBytes += data.Length;

            return true;
        }

        /// <summary>
        ///     Reads some bytes from this chunk.
        /// </summary>
        /// <remarks>
        ///     This routine will read at most the number of bytes which fit into the provided buffer.
        ///     Less bytes than this maximum might be read by a single call. The actual number is returned
        ///     by the routine.
        /// </remarks>
        /// <param name="offset">The relative start offset for the read.</param>
        /// <param name="buffer">The buffer into which the data is read.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the number of actually read bytes.</returns>
        public async ValueTask<int> Read(int offset, Memory<byte> buffer, CancellationToken cancellationToken)
        {
            int actualDataSize = this.UsedBytes - Specification.Chunk.HeaderSize;
            int availableData  = actualDataSize - offset;

            int readableSize = Math.Min(availableData, buffer.Length);
            
            await this.file.Read(this.DataOffset + offset, buffer.Slice(0, readableSize), cancellationToken).ConfigureAwait(false);

            return readableSize;
        }

        /// <summary>
        ///     Finalizes this chunk.
        /// </summary>
        /// <remarks>
        ///     After a chunk has been finalized, is must not be modified anymore. Any new data
        ///     should be written to the next chunk.
        /// </remarks>
        /// <param name="nextChunkOffset">The absolute file offset of the following chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the handle for this asynchronous operation.</returns>
        public async ValueTask Finalize(long nextChunkOffset, CancellationToken cancellationToken)
        {
            await UpdatePersistentHeader(cancellationToken).ConfigureAwait(false);
            
            await this.file.Write(this.NextChunkOffsetOffset, nextChunkOffset, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        ///     Updates the persistent version of the chunk header.
        /// </summary>
        /// <remarks>
        ///     This routine will not ensure that any chunk payload has actually been written
        ///     to the persistent storage. This should be ensured by other means
        ///     prior to calling this function.
        /// </remarks>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the handle for this asynchronous operation.</returns>
        public async ValueTask UpdatePersistentHeader(CancellationToken cancellationToken)
        {
            // Since finalize already writes the offset of the next chunk into the persistent storage,
            // and all other fields are immutable, we only need to update the number of used bytes.
            await this.file.Write(this.UsedBytesOffset, this.UsedBytes, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        ///     Flushes the data of this chunk to the persistent storage, i.e., ensuring that all pending writes
        ///     to this chunk are actually written to the persistent storage medium.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async ValueTask FlushData(CancellationToken cancellationToken)
        {
            // Since our current persistent storage backend does only support to flush
            // all pending changes, we have no choice but to perform a full flush for now.
            await this.file.Flush(cancellationToken);
        }

        /// <summary>
        ///     Writes some raw data to the chunk.
        /// </summary>
        /// <remarks>
        ///     This function does not differentiate between metadata and actual data. Its primary purpose
        ///     is to allow any upper layer, e.g., a cache, in the library to update the entire content of the chunk.
        ///
        ///     It should not be used to write actual data to the chunk.
        /// </remarks>
        /// <param name="offset">The chunk-relative offset to which the data should be written.</param>
        /// <param name="buffer">The buffer with the data.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>The number of bytes which are actually written.</returns>
        public async ValueTask<int> WriteRaw(int offset, Memory<byte> buffer, CancellationToken cancellationToken)
        {
            int availableStorage = this.ChunkSize;

            int writableSize = Math.Min(availableStorage, buffer.Length);
            
            await this.file.Write(this.StartOffset + offset, buffer.Slice(0, writableSize), cancellationToken).ConfigureAwait(false);

            return writableSize;
        }
        
        /// <summary>
        ///     Reads some raw data from the chunk.
        /// </summary>
        /// <remarks>
        ///     This function does not differentiate between metadata and actual data. Its primary purpose
        ///     is to allow any upper layer, e.g., a cache, in the library to read the entire content of the chunk.
        /// </remarks>
        /// <param name="offset">The chunk-relative offset from which the data should be read.</param>
        /// <param name="buffer">The buffer into which the data should be read.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>The number of bytes which are actually read.</returns>
        public async ValueTask<int> ReadRaw(int offset, Memory<byte> buffer, CancellationToken cancellationToken)
        {
            int availableData  = this.UsedBytes - offset;

            int readableSize = Math.Min(availableData, buffer.Length);
            
            await this.file.Read(this.StartOffset + offset, buffer.Slice(0, readableSize), cancellationToken).ConfigureAwait(false);

            return readableSize;
        }

        /// <summary>
        ///     The file which contains the chunk.
        /// </summary>
        private readonly RandomAccessFile file;
    }

    /// <summary>
    ///     An allocator for individual file chunks.
    /// </summary>
    public sealed class FileChunkAllocator : IDisposable
    {
        /// <summary>
        ///     Creates a new allocator for a specific file.
        /// </summary>
        /// <remarks>
        ///     Only one allocator should be used to operate on a specific file. The behavior is undefined otherwise.
        ///
        ///     This class is thread-safe.
        /// </remarks>
        /// <param name="file">The file which is used as the backing storage for all chunks allocated by this allocator.</param>
        public FileChunkAllocator(RandomAccessFile file)
        {
            this.file = file;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.allocatorSemaphore.Dispose();
        }

        /// <summary>
        ///     Allocates a new file chunk.
        /// </summary>
        /// <remarks>
        ///     The allocator might return a chunk with a stricter alignment than the one specified.
        /// </remarks>
        /// <param name="size">The total size of the chunk, including any metadata.</param>
        /// <param name="alignment">The minimum alignment of the chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the newly allocated file chunk.</returns>
        public async ValueTask<FileChunk> Allocate(int size, long alignment, CancellationToken cancellationToken)
        {
            using var guard = await SemaphoreGuard.Acquire(this.allocatorSemaphore, cancellationToken).ConfigureAwait(false);
            
            long startOffset = await this.file.Allocate(size, alignment, cancellationToken).ConfigureAwait(false);

            Debug.Assert(startOffset % alignment == 0,
                         "The allocated file chunk is not properly aligned.");
            
            return await FileChunk.Create(this.file, startOffset, size, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        ///     The binary semaphore used to serialize concurrent allocations.
        /// </summary>
        private readonly SemaphoreSlim allocatorSemaphore = new SemaphoreSlim(1, 1);

        /// <summary>
        ///     The file which is used as the backing storage for all chunks allocated by this allocator.
        /// </summary>
        private readonly RandomAccessFile file;
    }
}

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Qubus.Concurrency;
using Qubus.Errors;

namespace QTapeSharp
{
    /// <summary>
    ///     A handle for a file allowing random access.
    /// </summary>
    /// <remarks>
    ///     Any range of bytes which is accessed must be part of a successful allocation. The behavior of accessing non-allocated
    ///     bytes is undefined. The allocation must be completed before initiating the file operation accessing it since
    ///     this class will not automatically order these operations.
    /// </remarks>
    public sealed class RandomAccessFile : IDisposable
    {
        /// <summary>
        ///     Creates a new random-access file.
        /// </summary>
        /// <param name="path"></param>
        public RandomAccessFile(string path)
        {
            this.stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite, 4096, true);
        }

        /// <summary>
        ///     Writes a 64-bit integer value to a specific offset within the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent read or write operation.
        /// </remarks>
        /// <param name="offset">The file offset to which the data should be written.</param>
        /// <param name="value">The value to be written.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for the initiated asynchronous operation.</returns>
        public ValueTask Write(long offset, long value, CancellationToken cancellationToken)
        {
            return Write(offset, BitConverter.GetBytes(value), cancellationToken);
        }

        /// <summary>
        ///     Writes a 32-bit integer value to a specific offset within the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent read or write operation.
        /// </remarks>
        /// <param name="offset">The file offset to which the data should be written.</param>
        /// <param name="value">The value to be written.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for the initiated asynchronous operation.</returns>
        public ValueTask Write(long offset, int value, CancellationToken cancellationToken)
        {
            return Write(offset, BitConverter.GetBytes(value), cancellationToken);
        }

        /// <summary>
        ///     Writes a single-precision floating-point integer value to a specific offset within the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent read or write operation.
        /// </remarks>
        /// <param name="offset">The file offset to which the data should be written.</param>
        /// <param name="value">The value to be written.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for the initiated asynchronous operation.</returns>
        public ValueTask Write(long offset, float value, CancellationToken cancellationToken)
        {
            return Write(offset, BitConverter.GetBytes(value), cancellationToken);
        }

        /// <summary>
        ///     Writes a double-precision floating-point integer value to a specific offset within the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent read or write operation.
        /// </remarks>
        /// <param name="offset">The file offset to which the data should be written.</param>
        /// <param name="value">The value to be written.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for the initiated asynchronous operation.</returns>
        public ValueTask Write(long offset, double value, CancellationToken cancellationToken)
        {
            return Write(offset, BitConverter.GetBytes(value), cancellationToken);
        }

        /// <summary>
        ///     Writes a boolean value to a specific offset within the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent read or write operation.
        /// </remarks>
        /// <param name="offset">The file offset to which the data should be written.</param>
        /// <param name="value">The value to be written.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for the initiated asynchronous operation.</returns>
        public ValueTask Write(long offset, bool value, CancellationToken cancellationToken)
        {
            return Write(offset, BitConverter.GetBytes(value), cancellationToken);
        }

        /// <summary>
        ///     Writes a sequence of bytes to a specific offset within the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent read or write operation.
        /// </remarks>
        /// <param name="offset">The file offset to which the data should be written.</param>
        /// <param name="data">The sequence of bytes to be written.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for the initiated asynchronous operation.</returns>
        public async ValueTask Write(long offset, ReadOnlyMemory<byte> data, CancellationToken cancellationToken)
        {
            using var guard = await SemaphoreGuard.Acquire(this.fileSemaphore, cancellationToken).ConfigureAwait(false);

            this.stream.Seek(offset, SeekOrigin.Begin);

            await this.stream.WriteAsync(data, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        ///     Reads a 64-bit integer value stored at a specific offset from the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent write operation. Read operations might overlap
        ///     arbitrarily.
        /// </remarks>
        /// <param name="offset">The file offset from which the data should be read.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the read value.</returns>
        public async ValueTask<long> ReadInt64(long offset, CancellationToken cancellationToken)
        {
            var buffer = new Memory<byte>(new byte[sizeof(long)]);

            await Read(offset, buffer, cancellationToken).ConfigureAwait(false);

            return MemoryMarshal.Read<long>(buffer.Span);
        }

        /// <summary>
        ///     Reads a 32-bit integer value stored at a specific offset from the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent write operation. Read operations might overlap
        ///     arbitrarily.
        /// </remarks>
        /// <param name="offset">The file offset from which the data should be read.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the read value.</returns>
        public async ValueTask<int> ReadInt32(long offset, CancellationToken cancellationToken)
        {
            var buffer = new Memory<byte>(new byte[sizeof(int)]);

            await Read(offset, buffer, cancellationToken).ConfigureAwait(false);

            return MemoryMarshal.Read<int>(buffer.Span);
        }

        /// <summary>
        ///     Reads a double-precision floating-point value stored at a specific offset from the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent write operation. Read operations might overlap
        ///     arbitrarily.
        /// </remarks>
        /// <param name="offset">The file offset from which the data should be read.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>The read value.</returns>
        public async ValueTask<double> ReadDouble(long offset, CancellationToken cancellationToken)
        {
            var buffer = new Memory<byte>(new byte[sizeof(double)]);

            await Read(offset, buffer, cancellationToken).ConfigureAwait(false);

            return MemoryMarshal.Read<double>(buffer.Span);
        }

        /// <summary>
        ///     Reads a single-precision floating-point value stored at a specific offset from the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent write operation. Read operations might overlap
        ///     arbitrarily.
        /// </remarks>
        /// <param name="offset">The file offset from which the data should be read.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the read value.</returns>
        public async ValueTask<float> ReadFloat(long offset, CancellationToken cancellationToken)
        {
            var buffer = new Memory<byte>(new byte[sizeof(float)]);

            await Read(offset, buffer, cancellationToken).ConfigureAwait(false);

            return MemoryMarshal.Read<float>(buffer.Span);
        }

        /// <summary>
        ///     Reads a boolean value stored at a specific offset from the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent write operation. Read operations might overlap
        ///     arbitrarily.
        /// </remarks>
        /// <param name="offset">The file offset from which the data should be read.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the read value.</returns>
        public async ValueTask<bool> ReadBoolean(long offset, CancellationToken cancellationToken)
        {
            var buffer = new Memory<byte>(new byte[sizeof(bool)]);

            await Read(offset, buffer, cancellationToken).ConfigureAwait(false);

            return MemoryMarshal.Read<bool>(buffer.Span);
        }

        /// <summary>
        ///     Reads a sequence of bytes stored at a specific offset from the file.
        /// </summary>
        /// <remarks>
        ///     This operation is safe to initiate concurrently as long as the affected file region is
        ///     does not overlap with any other concurrent write operation. Read operations might overlap
        ///     arbitrarily.
        /// </remarks>
        /// <param name="offset">The file offset from which the data should be read.</param>
        /// <param name="data">The buffer to which the byte sequence should be written.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for the initiated asynchronous operation.</returns>
        public async ValueTask Read(long offset, Memory<byte> data, CancellationToken cancellationToken)
        {
            using var guard = await SemaphoreGuard.Acquire(this.fileSemaphore, cancellationToken).ConfigureAwait(false);

            this.stream.Seek(offset, SeekOrigin.Begin);

            await this.stream.ReadAsync(data, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        ///     Allocates a region of the file.
        /// </summary>
        /// <remarks>
        ///     The purpose of this function is to ensure that the suitable region spanning the specified size is actually accessible
        ///     and that different allocations do not overlap, i.e., concurrent writes to these regions are safe.
        ///
        ///     The position of the allocated region within the file is unspecified.
        /// 
        ///     This function is thread-safe.
        /// </remarks>
        /// <param name="size">The usable size of the specified region.</param>
        /// <param name="alignment">The alignment of the usable portion of the allocated region.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the file offset at which the usable part of the allocation starts.</returns>
        public async ValueTask<long> Allocate(long size, long alignment, CancellationToken cancellationToken)
        {
            using var guard = await SemaphoreGuard.Acquire(this.fileSemaphore, cancellationToken).ConfigureAwait(false);

            long allocatedStorageEndOffset = this.stream.Length;

            long nextAlignedOffset = calculateNextAlignedOffset(allocatedStorageEndOffset, alignment);

            // Allocate a chunk from 'nextAlignedOffset' to 'nextAlignedOffset + size'.
            this.stream.SetLength(nextAlignedOffset + size);

            return nextAlignedOffset;
        }

        /// <summary>
        ///     Flushes any pending changes to persistent storage.
        /// </summary>
        /// <remarks>
        ///     The actual level of persistence does depend on the backing storage.
        /// </remarks>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for this asynchronous operation.</returns>
        public Task Flush(CancellationToken cancellationToken)
        {
            return this.stream.FlushAsync(cancellationToken);
        }

        /// <summary>
        ///     Specifies if the file is empty or not.
        /// </summary>
        public bool IsEmpty => this.stream.Length == 0;

        /// <inheritdoc />
        public void Dispose()
        {
            this.fileSemaphore.Dispose();
            this.stream.Dispose();
        }

        /// <summary>
        ///     Calculates the next properly aligned offset starting from the specified start offset.
        /// </summary>
        /// <param name="startOffset">The start offset.</param>
        /// <param name="alignment">The requested alignment.</param>
        /// <returns>the offset satisfying the requirements.</returns>
        private static long calculateNextAlignedOffset(long startOffset, long alignment)
        {
            long offsetFromPreviousAlignedOffset = startOffset % alignment;

            // If the start offset is already aligned, we can just return it.
            if (offsetFromPreviousAlignedOffset == 0)
            {
                return startOffset;
            }

            long previousAlignedOffset = startOffset - offsetFromPreviousAlignedOffset;

            Debug.Assert(previousAlignedOffset % alignment == 0, "This position must be aligned.");

            // Add the alignment to the already aligned offset. This ensures that the offset is both aligned and just larger than
            // the start offset.
            long alignedOffset = previousAlignedOffset + alignment;

            Release.Assert(alignedOffset > startOffset, "The calculated offset must be larger than the start offset.");
            Release.Assert(alignedOffset % alignment == 0, "This position must be aligned.");

            return alignedOffset;
        }

        /// <summary>
        ///     The binary semaphore used to serialize access to the file if necessary.
        /// </summary>
        private readonly SemaphoreSlim fileSemaphore = new SemaphoreSlim(1, 1);

        /// <summary>
        ///     The actual file handle.
        /// </summary>
        private readonly FileStream stream;
    }
}

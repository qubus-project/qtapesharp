using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Qubus.Errors;
using Qubus.Lifetime;

namespace QTapeSharp
{
    /// <summary>
    ///     An iterator marking a specific position within a raw track.
    /// </summary>
    /// <remarks>
    ///     The iterator behaves as if a raw track is actually a continuous stream of bytes.
    /// </remarks>
    internal sealed class RawTrackIterator : IAsyncDisposable
    {
        /// <summary>
        ///     Creates a new iterator at a specified byte offset within the track.
        /// </summary>
        /// <param name="headChunk">The chunk into which the iterator is initially referencing.</param>
        /// <param name="chunkOffset">The initial byte offset within this chunk.</param>
        /// <param name="accessManager">The chunk access manager responsible for the referenced raw track.</param>
        public RawTrackIterator(AsyncArc<ActiveChunk> headChunk, int chunkOffset, ChunkAccessManager accessManager)
        {
            this.accessManager = accessManager;

            this.currentChunk = headChunk;
            this.chunkOffset  = chunkOffset;
        }

        /// <summary>
        ///     Tries to read a certain number of bytes from the raw track, starting at the current position of the iterator.
        /// </summary>
        /// <param name="buffer">The buffer into which the sequence of bytes is read.</param>
        /// <param name="size">The number of bytes to read.</param>
        /// <param name="cancellationToken">The cancellation token fpr this operation.</param>
        /// <returns>true if the operation was successful, false otherwise.</returns>
        public async ValueTask<bool> TryRead(Memory<byte> buffer, int size, CancellationToken cancellationToken)
        {
            Release.Assert(size <= buffer.Length, "The size of the buffer is insufficient.");

            int overallReadBytes = 0;

            for (;;)
            {
                int bytesToRead = size - overallReadBytes;

                int readBytes = this.currentChunk.Value.Read(this.chunkOffset,
                                                             buffer.Span.Slice(overallReadBytes, bytesToRead));

                overallReadBytes += readBytes;
                this.chunkOffset += readBytes;

                if (overallReadBytes == size)
                    return true;

                await using AsyncArc<ActiveChunk>? nextChunk =
                    await this.accessManager.QueryNextChunk(this.currentChunk.Value, cancellationToken)
                              .ConfigureAwait(false);

                if (nextChunk == null)
                    return false;

                await this.currentChunk.AssumeOwnershipFrom(nextChunk).ConfigureAwait(false);
                this.chunkOffset = 0;
            }
        }

        /// <summary>
        ///     Tries to read a 64-bit integer from the raw stream., starting at the current position of the iterator.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token fpr this operation.</param>
        /// <returns>the read value if the operation was successful, null otherwise.</returns>
        public async ValueTask<long?> TryReadInt64(CancellationToken cancellationToken)
        {
            var buffer = new Memory<byte>(new byte[sizeof(long)]);

            if (!await TryRead(buffer, sizeof(long), cancellationToken).ConfigureAwait(false))
            {
                return null;
            }

            return MemoryMarshal.Read<long>(buffer.Span);
        }

        /// <summary>
        ///     Tries to read a 32-bit integer from the raw stream., starting at the current position of the iterator.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token fpr this operation.</param>
        /// <returns>the read value if the operation was successful, null otherwise.</returns>
        public async ValueTask<int?> TryReadInt32(CancellationToken cancellationToken)
        {
            var buffer = new Memory<byte>(new byte[sizeof(int)]);

            if (!await TryRead(buffer, sizeof(int), cancellationToken).ConfigureAwait(false))
            {
                return null;
            }

            return MemoryMarshal.Read<int>(buffer.Span);
        }

        /// <inheritdoc />
        public ValueTask DisposeAsync()
        {
            return this.currentChunk.DisposeAsync();
        }

        /// <summary>
        ///     The chunk access manager responsible for the referenced raw track.
        /// </summary>
        private readonly ChunkAccessManager accessManager;

        /// <summary>
        ///     The current chunk referenced by this iterator.
        /// </summary>
        private readonly AsyncArc<ActiveChunk> currentChunk;
        
        /// <summary>
        ///     The byte offset within the current chunk.
        /// </summary>
        private int                   chunkOffset;
    }

    /// <summary>
    ///     A continuous sequence of bytes stored in a QTape file.
    /// </summary>
    /// <remarks>
    ///     The bytes are actually stored as a linked list of chunks within the file.
    ///     The primary purpose of this type is to abstract this away.
    /// </remarks>
    internal sealed class RawTrack : IAsyncDisposable
    {
        /// <summary>
        ///     Creates a new raw track.
        /// </summary>
        /// <param name="accessManager">The chunk access manager used to manage the chunks belonging to this track.</param>
        /// <param name="cancellationToken">The cancellation token für this operation.</param>
        /// <returns>an owned reference to the new track.</returns>
        /// <exception cref="Exception">if the track could not be created.</exception>
        public static async ValueTask<AsyncOwner<RawTrack>> Create(ChunkAccessManager accessManager,
                                                                   CancellationToken  cancellationToken)
        {
            await using AsyncArc<ActiveChunk> headChunk =
                await accessManager.Allocate(RawTrackChunkSize, Specification.AtomicAlignment, cancellationToken)
                                   .ConfigureAwait(false);

            // Write the raw track signature.
            var buffer = new Memory<byte>(new byte[sizeof(long)]);

            long magicBytes = Specification.RawTrackMagicBytes;

            MemoryMarshal.Write(buffer.Span, ref magicBytes);

            int writtenBytes = headChunk.Value.Write(buffer.Span);

            if (writtenBytes != buffer.Length)
            {
                throw new Exception("The signature for this raw track could not be written.");
            }

            await using AsyncArc<ActiveChunk> currentChunk = headChunk.Acquire();

            for (;;)
            {
                AsyncArc<ActiveChunk>? nextChunk = await accessManager
                                                         .QueryNextChunk(currentChunk.Value, cancellationToken)
                                                         .ConfigureAwait(false);

                if (nextChunk == null)
                    break;

                await currentChunk.AssumeOwnershipFrom(nextChunk).ConfigureAwait(false);
            }

            return new AsyncOwner<RawTrack>(new RawTrack(accessManager, headChunk.Move(), currentChunk.Move()));
        }

        /// <summary>
        ///     Opens an existing track.
        /// </summary>
        /// <param name="offset">The absolute file offset at which the track's first chunk is located.</param>
        /// <param name="accessManager">The chunk access manager used to manage the chunks belonging to this track.</param>
        /// <param name="cancellationToken">The cancellation token für this operation.</param>
        /// <returns>an owned reference to the track.</returns>
        /// <exception cref="Exception">if the track could not be opened.</exception>
        public static async ValueTask<AsyncOwner<RawTrack>> Open(long               offset,
                                                                 ChunkAccessManager accessManager,
                                                                 CancellationToken  cancellationToken)
        {
            await using AsyncArc<ActiveChunk> headChunk =
                await accessManager.Open(offset, cancellationToken).ConfigureAwait(false);

            var buffer = new Memory<byte>(new byte[sizeof(long)]);

            long readBytes = headChunk.Value.Read(0, buffer.Span);

            if (readBytes != buffer.Length)
            {
                throw new Exception("The entire raw track signature could not be read.");
            }

            if (MemoryMarshal.Read<long>(buffer.Span) != Specification.RawTrackMagicBytes)
            {
                throw new Exception("The signature of the raw track is invalid. The offset of the raw track" +
                                    "might be wrong.");
            }

            await using AsyncArc<ActiveChunk> currentChunk = headChunk.Acquire();

            for (;;)
            {
                await using AsyncArc<ActiveChunk>? nextChunk =
                    await accessManager.QueryNextChunk(currentChunk.Value, cancellationToken)
                                       .ConfigureAwait(false);

                if (nextChunk == null)
                    break;

                await currentChunk.AssumeOwnershipFrom(nextChunk).ConfigureAwait(false);
            }

            return new AsyncOwner<RawTrack>(new RawTrack(accessManager, headChunk.Move(), currentChunk.Move()));
        }

        /// <summary>
        ///     Creates a new track object.
        /// </summary>
        /// <param name="accessManager">The chunk access manager used to manage the chunks belonging to this track.</param>
        /// <param name="headChunk">The head chunk of the track.</param>
        /// <param name="lastChunk">The last chunk of the track.</param>
        private RawTrack(ChunkAccessManager    accessManager,
                         AsyncArc<ActiveChunk> headChunk,
                         AsyncArc<ActiveChunk> lastChunk)
        {
            this.accessManager = accessManager;

            this.headChunk = headChunk;
            this.lastChunk = lastChunk;
        }

        /// <summary>
        ///     The file offset at which the first chunk of the track is located.
        /// </summary>
        public long StartOffset => this.headChunk.Value.StartOffset;

        /// <summary>
        ///     Appends a 32-bit integer to the track.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a file reference marking the position of the written data within the stream.</returns>
        public ValueTask<FileReference> Write(int value, CancellationToken cancellationToken)
        {
            return Write(BitConverter.GetBytes(value), cancellationToken);
        }

        /// <summary>
        ///     Appends a 64-bit integer to the track.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a file reference marking the position of the written data within the stream.</returns>
        public ValueTask<FileReference> Write(long value, CancellationToken cancellationToken)
        {
            return Write(BitConverter.GetBytes(value), cancellationToken);
        }

        /// <summary>
        ///     Appends a single-precision floating point number to the track.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a file reference marking the position of the written data within the stream.</returns>
        public ValueTask<FileReference> Write(float value, CancellationToken cancellationToken)
        {
            return Write(BitConverter.GetBytes(value), cancellationToken);
        }

        /// <summary>
        ///     Appends a double-precision floating point number to the track.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a file reference marking the position of the written data within the stream.</returns>
        public ValueTask<FileReference> Write(double value, CancellationToken cancellationToken)
        {
            return Write(BitConverter.GetBytes(value), cancellationToken);
        }

        /// <summary>
        ///     Appends a boolean value to the track.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a file reference marking the position of the written data within the stream.</returns>
        public ValueTask<FileReference> Write(bool value, CancellationToken cancellationToken)
        {
            return Write(BitConverter.GetBytes(value), cancellationToken);
        }

        /// <summary>
        ///     Appends a sequence of bytes to the track.
        /// </summary>
        /// <param name="data">The sequence of bytes.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a file reference marking the position of the written data within the stream.</returns>
        public async ValueTask<FileReference> Write(ReadOnlyMemory<byte> data, CancellationToken cancellationToken)
        {
            await using AsyncArc<ActiveChunk> startChunk  = this.lastChunk.Acquire();
            long                              startOffset = startChunk.Value.UsedBytes;

            await using AsyncArc<ActiveChunk> currentChunk = startChunk.Acquire();

            for (;;)
            {
                bool hasBeenWritten = currentChunk.Value.TryWriteAtomically(data.Span);

                if (!hasBeenWritten)
                {
                    await using AsyncArc<ActiveChunk> newChunk = await this.accessManager.Allocate(RawTrackChunkSize,
                                                                                                   Specification
                                                                                                       .AtomicAlignment,
                                                                                                   cancellationToken);

                    await currentChunk.Value.Finalize(newChunk.Value.StartOffset, cancellationToken);

                    await currentChunk.AssumeOwnershipFrom(newChunk);
                    this.lastChunk = currentChunk.Acquire();
                }
                else
                {
                    break;
                }
            }

            return new FileReference(startChunk.Move(), startOffset);
        }

        /// <summary>
        ///     Streams the content of the track a sequence of chunks with a specified size.
        /// </summary>
        /// <remarks>
        ///     The size of these chunks does not need to correspond to the size of the in-file chunks.
        /// </remarks>
        /// <param name="offset">The track offset at which the stream is starting in bytes.</param>
        /// <param name="size">The size if each streamed chunk in bytes.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the sequence of chunks making up the content of this track.</returns>
        /// <exception cref="Exception">if the specified stream would not be valid.</exception>
        public async IAsyncEnumerable<ReadOnlyMemory<byte>> Stream(long offset,
                                                                   int  size,
                                                                   [EnumeratorCancellation]
                                                                   CancellationToken cancellationToken = default)
        {
            long                              remainingOffset = offset + SignatureSize;
            await using AsyncArc<ActiveChunk> currentChunk    = this.headChunk.Acquire();

            for (;;)
            {
                if (remainingOffset < currentChunk.Value.UsedBytes)
                {
                    break;
                }

                remainingOffset -= currentChunk.Value.UsedBytes;
                await using AsyncArc<ActiveChunk>? nextChunk =
                    await this.accessManager.QueryNextChunk(currentChunk.Value, cancellationToken);

                if (nextChunk == null)
                {
                    throw new Exception("Invalid offset.");
                }

                await currentChunk.AssumeOwnershipFrom(nextChunk);
            }

            // After the seek, the current chunk is the first one with actual data.

            // The remaining offset from the seek is also the chunk offset for the first chunk.
            // Since the remaining offset points into a single chunk it must fit into a 32-bit int.
            int chunkOffset = Integers.RequireSafeConversionToInt32(remainingOffset);

            var buffer = new Memory<byte>(new byte[size]);

            for (;;)
            {
                int overallReadBytes = 0;

                for (;;)
                {
                    int bytesWhichNeedToBeRead = buffer.Length - overallReadBytes;

                    Release.Assert(bytesWhichNeedToBeRead > 0, "Invalid amount of bytes.");

                    int readBytes = currentChunk.Value.Read(chunkOffset,
                                                                  buffer.Span.Slice(
                                                                      overallReadBytes,
                                                                      bytesWhichNeedToBeRead));

                    overallReadBytes += readBytes;
                    chunkOffset      += readBytes;

                    if (readBytes < bytesWhichNeedToBeRead)
                    {
                        await using AsyncArc<ActiveChunk>? nextChunk =
                            await this.accessManager.QueryNextChunk(currentChunk.Value, cancellationToken);

                        if (nextChunk == null)
                        {
                            // If we could not read everything and we have no chunk to continue with, an illegal partial read
                            // has occured.
                            if (overallReadBytes > 0)
                            {
                                throw new Exception("A partial read has occured. The track is missing data.");
                            }

                            // Otherwise, we have read everything within this track and are done.
                            yield break;
                        }

                        await currentChunk.AssumeOwnershipFrom(nextChunk);
                        chunkOffset = 0;
                    }

                    if (overallReadBytes == buffer.Length)
                    {
                        break;
                    }
                }

                yield return buffer;
            }
        }

        /// <summary>
        ///     Creates an iterator for this track starting at its first byte.
        /// </summary>
        /// <returns>the iterator.</returns>
        public RawTrackIterator Iterate()
        {
            return new RawTrackIterator(this.headChunk.Acquire(), SignatureSize, this.accessManager);
        }

        /// <summary>
        ///     Updates the persistent metadata for this track.
        /// </summary>
        /// <remarks>
        ///     This includes the metadata of all chunk constituting this track.
        ///
        ///     This metadata flush is usually used as part of a full data flush
        ///     to get the persistent metadata in sync with the already written data.
        ///     It should never be used on its own since otherwise the stored metadata
        ///     might no represent the actual state of the persistent storage.
        /// </remarks>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for this asynchronous operation.</returns>
        public ValueTask UpdatePersistentMetaData(CancellationToken cancellationToken)
        {
            // We only need to update the persistent header of the last chunk since
            // all other updates have already been performed during the finalization of every other chunk
            // in this track.
            return this.lastChunk.Value.UpdatePersistentHeader(cancellationToken);
        }

        /// <inheritdoc />
        public async ValueTask DisposeAsync()
        {
            await this.headChunk.DisposeAsync();
            await this.lastChunk.DisposeAsync();
        }

        /// <summary>
        ///     The size of the raw track signature.
        /// </summary>
        private const int SignatureSize = sizeof(long);

        /// <summary>
        ///     The chunk size chosen for this track.
        /// </summary>
        private const int RawTrackChunkSize = Specification.AtomicAlignment;

        /// <summary>
        ///     The chunk access manager used to manage the chunks belonging to this track.
        /// </summary>
        private readonly ChunkAccessManager accessManager;

        /// <summary>
        ///     The head chunk of the track.
        /// </summary>
        private AsyncArc<ActiveChunk> headChunk;
        
        /// <summary>
        ///     The last chunk of the track.
        /// </summary>
        private AsyncArc<ActiveChunk> lastChunk;
    }
}

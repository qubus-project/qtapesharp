﻿using System.ComponentModel;
using Qubus.Errors;

namespace QTapeSharp
{
    /// <summary>
    ///     The base exception class which should be used by all exceptions thrown by QTapeSharp.
    /// </summary>
    public class Exception : Qubus.Errors.Exception
    {
        /// <summary>
        ///     Creates a default exception.
        /// </summary>
        public Exception()
        {
        }

        /// <summary>
        ///     Creates an exception carrying a message describing the error.
        /// </summary>
        /// <param name="message">The diagnostic message describing the error.</param>
        public Exception(string message)
            : base(message)
        {
        }

        /// <summary>
        ///     Creates an exception carrying a message describing the error and the exception triggering this error.
        /// </summary>
        /// <param name="message">The diagnostic message describing the error.</param>
        /// <param name="e">The exception triggering this error.</param>
        public Exception(string message, System.Exception e)
            : base(message, e)
        {
        }
    }
}

using System;
using System.Threading.Tasks;
using Qubus.Lifetime;

namespace QTapeSharp
{
    /// <summary>
    ///     A reference to a specific file position, encoded as a relative offset within a specific chunk. 
    /// </summary>
    internal readonly struct FileReference : IAsyncDisposable
    {
        /// <summary>
        ///     Creates a new file reference.
        /// </summary>
        /// <remarks>
        ///     The file reference will acquire the ownership of chunk from the caller.
        /// </remarks>
        /// <param name="chunk">The chunk which will be referenced.</param>
        /// <param name="chunkOffset">The actual data offset within this chunk.</param>
        public FileReference(AsyncArc<ActiveChunk> chunk, long chunkOffset)
        {
            this.Chunk       = chunk;
            this.ChunkOffset = chunkOffset;
        }

        /// <summary>
        ///     The chunk which is referenced.
        /// </summary>
        public readonly AsyncArc<ActiveChunk> Chunk;
        
        /// <summary>
        ///     The relative offset within the referenced chunk.
        /// </summary>
        public readonly long                  ChunkOffset;

        /// <summary>
        ///     The absolute file offset of the position specified by this reference.
        /// </summary>
        public long AbsoluteOffset => this.Chunk.Value.CalculateAbsoluteOffset(this.ChunkOffset);

        /// <inheritdoc />
        public ValueTask DisposeAsync()
        {
            return this.Chunk.DisposeAsync();
        }
    }
}

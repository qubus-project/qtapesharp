using System;

namespace QTapeSharp
{
    /// <summary>
    ///     A serializer for a specific event type.
    /// </summary>
    public interface IEventSerializer
    {
        /// <summary>
        ///     The type of the serialized event.
        /// </summary>
        Type EventType { get; }

        /// <summary>
        ///     The size of the serialized event in bytes.
        /// </summary>
        int EventSize { get; }

        /// <summary>
        ///     The name of the serialized event type.
        /// </summary>
        string TypeName { get; }

        /// <summary>
        ///     Serializes an event.
        /// </summary>
        /// <param name="payload">The event payload.</param>
        /// <param name="buffer">The sequenced a bytes representing the serialized event.</param>
        /// <exception cref="Exception">if type of the event is not supported by this serializer or
        ///                             the buffer could not fit the serialized representation
        /// </exception>
        void Serialize(object payload, Span<byte> buffer);

        /// <summary>
        ///     Deserializes an event.
        /// </summary>
        /// <param name="buffer">The sequence of bytes representing the serialized event.</param>
        /// <returns>the deserialized event.</returns>
        /// <exception cref="Exception">if type of the event is not supported by this serializer or
        ///                             if the provided buffer does not contain a valid representation of the event type.
        /// </exception>
        object Deserialize(ReadOnlySpan<byte> buffer);
    }
}

using System;
using System.Globalization;

namespace QTapeSharp
{
    /// <summary>
    ///     An event with an untyped payload.
    /// </summary>
    /// <remarks>
    ///     The payload is encoded as a sequence of bytes.
    /// </remarks>
    public sealed class Event
    {
        /// <summary>
        ///     Creates a new event.
        /// </summary>
        /// <param name="timestamp">The timestamp for the event.</param>
        /// <param name="payload">The payload of the event.</param>
        public Event(DateTime timestamp, ReadOnlyMemory<byte> payload)
        {
            this.Timestamp = timestamp;
            this.payload   = payload;
        }

        /// <summary>
        ///     The timestamp for the event.
        /// </summary>
        public DateTime Timestamp { get; }

        /// <summary>
        ///     The payload of the event.
        /// </summary>
        public ReadOnlySpan<byte> Payload => this.payload.Span;

        /// <summary>
        ///     The payload of the event.
        /// </summary>
        private readonly ReadOnlyMemory<byte> payload;
    }

    /// <summary>
    ///     An event with a typed payload.
    /// </summary>
    /// <typeparam name="T">The type of the payload.</typeparam>
    public sealed class Event<T> where T : struct
    {
        /// <summary>
        ///     Creates a new event.
        /// </summary>
        /// <param name="timestamp">The timestamp for the event.</param>
        /// <param name="payload">The payload of the event.</param>
        public Event(DateTime timestamp, T payload)
        {
            this.Timestamp = timestamp;
            this.Payload   = payload;
        }

        /// <summary>
        ///     The timestamp for the event.
        /// </summary>
        public DateTime Timestamp { get; }

        /// <summary>
        ///     The payload of the event.
        /// </summary>
        public T Payload { get; }

        /// <inheritdoc />
        public override bool Equals(object? obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (!(obj is Event<T> ev))
            {
                return false;
            }

            return this.Timestamp.Equals(ev.Timestamp) && this.Payload.Equals(ev.Payload);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            var hashCode = new HashCode();

            hashCode.Add(this.Timestamp);
            hashCode.Add(this.Payload);

            return hashCode.ToHashCode();
        }

        /// <inheritdoc />
        public override string ToString()
        {
            string renderedTimestamp = this.Timestamp.ToString(CultureInfo.InvariantCulture);

            string? renderedPayload = this.Payload.ToString();

            if (renderedPayload == null)
            {
                return renderedTimestamp;
            }

            return $"{renderedPayload} @ {renderedTimestamp}";
        }
    }
}

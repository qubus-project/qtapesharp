using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Qubus.Lifetime;

namespace QTapeSharp
{
    /// <summary>
    ///     The type if the track,
    /// </summary>
    /// <remarks>
    ///     The set of available types and their ids is part of the file format specification
    ///     and must not be altered without introducing a new major version. 
    /// </remarks>
    public enum TrackType : int
    {
        /// <summary>
        ///     A primitive track.
        /// </summary>
        PrimitiveTrack = 0 
    }
    
    /// <summary>
    ///     A track storing a sequence of events.
    /// </summary>
    public sealed class Track : IAsyncDisposable
    {
        /// <inheritdoc />
        public ValueTask DisposeAsync()
        {
            return this.dataTrack.DisposeAsync();
        }

        /// <summary>
        ///     The id of the track.
        /// </summary>
        public string Id { get; }

        /// <summary>
        ///     The type of the stored events.
        /// </summary>
        public Type EventType => this.serializer.EventType;

        /// <summary>
        ///     Appends an event.
        /// </summary>
        /// <param name="timestamp">The timestamp of the event.</param>
        /// <param name="data">The payload of the event.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for this asynchronous operation.</returns>
        public async ValueTask AppendEvent(DateTime             timestamp,
                                           ReadOnlyMemory<byte> data,
                                           CancellationToken    cancellationToken)
        {
            long normalizedTimeStamp = Specification.ToTicks(timestamp);

            await this.dataTrack.Value.Write(normalizedTimeStamp, cancellationToken).ConfigureAwait(false);
            await this.dataTrack.Value.Write(data, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        ///     Queries the entire event stream.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the stream of all events.</returns>
        public async IAsyncEnumerable<Event> QueryEverything(
            [EnumeratorCancellation] CancellationToken cancellationToken = default)
        {
            await foreach (ReadOnlyMemory<byte> eventData in this
                                                             .dataTrack.Value.Stream(0, sizeof(long) + this.eventSize)
                                                             .WithCancellation(cancellationToken)
                                                             .ConfigureAwait(false))
            {
                // Split the event data into the timestamp and the actual payload.
                long timestampInTicks = MemoryMarshal.Read<long>(eventData.Span.Slice(0, sizeof(long)));

                yield return new Event(Specification.ToDateTime(timestampInTicks), eventData.Slice(sizeof(long)));
            }
        }

        /// <summary>
        ///     Queries the event stream between two specified times.
        /// </summary>
        /// <remarks>
        ///     Only events with timestamps within the specified range, including the boundaries,
        ///     will be enumerated by this operation.
        /// </remarks>
        /// <param name="start">The start time of the queried event stream.</param>
        /// <param name="end">The end time of the queried event stream.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the stream of selected events.</returns>
        public async IAsyncEnumerable<Event> QueryRange(DateTime start,
                                                        DateTime end,
                                                        [EnumeratorCancellation] CancellationToken cancellationToken =
                                                            default)
        {
            await foreach (ReadOnlyMemory<byte> eventData in this
                                                             .dataTrack.Value.Stream(0, sizeof(long) + this.eventSize)
                                                             .WithCancellation(cancellationToken)
                                                             .ConfigureAwait(false))
            {
                // Split the event data into the timestamp and the actual payload.
                long timestampInTicks = MemoryMarshal.Read<long>(eventData.Span.Slice(0, sizeof(long)));

                DateTime timestamp = Specification.ToDateTime(timestampInTicks);

                if (timestamp < start)
                {
                    continue;
                }

                if (timestamp > end)
                {
                    yield break;
                }

                yield return new Event(timestamp, eventData.Slice(sizeof(long)));
            }
        }

        /// <summary>
        ///     Converts this untyped track to a typed one.
        /// </summary>
        /// <typeparam name="T">The supposed payload type of the track.</typeparam>
        /// <returns>the typed track.</returns>
        /// <exception cref="Exception">if the track could not be converted.</exception>
        public Track<T> Cast<T>() where T : struct
        {
            if (typeof(T) != this.serializer.EventType)
            {
                throw new Exception($"This track contains events of type '{this.serializer.EventType.FullName}' " +
                                    $"and not of type '{typeof(T).FullName}'.");
            }
            
            return new Track<T>(this, this.serializer);
        }
        
        /// <summary>
        ///     Updates the persistent metadata for this track.
        /// </summary>
        /// <remarks>
        ///     This includes the metadata of all chunk constituting this track.
        ///
        ///     This metadata flush is usually used as part of a full data flush
        ///     to get the persistent metadata in sync with the already written data.
        ///     It should never be used on its own since otherwise the stored metadata
        ///     might no represent the actual state of the persistent storage.
        /// </remarks>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for this asynchronous operation.</returns>
        public ValueTask UpdatePersistentMetaData(CancellationToken cancellationToken)
        {
            return this.dataTrack.Value.UpdatePersistentMetaData(cancellationToken);
        }

        /// <summary>
        ///     Creates a new track.
        /// </summary>
        /// <param name="id">The id of this track.</param>
        /// <param name="serializer">The serializer for the events.</param>
        /// <param name="dataTrack">The raw track used to store the event data.</param>
        internal Track(string id, IEventSerializer serializer, AsyncOwner<RawTrack> dataTrack)
        {
            this.Id = id;
            this.serializer = serializer;
            this.dataTrack = dataTrack;
        }

        /// <summary>
        ///     The size of each event in bytes.
        /// </summary>
        private int eventSize => this.serializer.EventSize;

        /// <summary>
        ///     The serializer for the events.
        /// </summary>
        private readonly IEventSerializer serializer;
        
        /// <summary>
        ///     The raw track used to store the event data.
        /// </summary>
        private readonly AsyncOwner<RawTrack> dataTrack;
    }

    /// <summary>
    ///     A track storing a typed sequence of events.
    /// </summary>
    public sealed class Track<T> : IAsyncDisposable where T : struct
    {
        /// <inheritdoc />
        public ValueTask DisposeAsync()
        {
            return this.genericTrack.DisposeAsync();
        }

        /// <summary>
        ///     The id of the track.
        /// </summary>
        public string Id => this.genericTrack.Id;

        /// <summary>
        ///     Appends an event.
        /// </summary>
        /// <param name="timestamp">The timestamp of the event.</param>
        /// <param name="value">The payload of the event.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>a handle for this asynchronous operation.</returns>
        public ValueTask AppendEvent(DateTime timestamp, T value, CancellationToken cancellationToken)
        {
            var buffer = new Memory<byte>(new byte[this.serializer.EventSize]);

            this.serializer.Serialize(value, buffer.Span);

            return this.genericTrack.AppendEvent(timestamp, buffer, cancellationToken);
        }

        /// <summary>
        ///     Queries the entire event stream.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the stream of all events.</returns>
        public async IAsyncEnumerable<Event<T>> QueryEverything(
            [EnumeratorCancellation] CancellationToken cancellationToken = default)
        {
            await foreach (Event storedEvent in this.genericTrack.QueryEverything()
                                                    .WithCancellation(cancellationToken)
                                                    .ConfigureAwait(false))
            {
                yield return new Event<T>(storedEvent.Timestamp, MemoryMarshal.Read<T>(storedEvent.Payload));
            }
        }

        /// <summary>
        ///     Queries the event stream between two specified times.
        /// </summary>
        /// <remarks>
        ///     Only events with timestamps within the specified range, including the boundaries,
        ///     will be enumerated by this operation.
        /// </remarks>
        /// <param name="start">The start time of the queried event stream.</param>
        /// <param name="end">The end time of the queried event stream.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the stream of selected events.</returns>
        public async IAsyncEnumerable<Event<T>> QueryRange(DateTime start,
                                                           DateTime end,
                                                           [EnumeratorCancellation]
                                                           CancellationToken cancellationToken = default)
        {
            await foreach (Event storedEvent in this.genericTrack.QueryRange(start, end)
                                                    .WithCancellation(cancellationToken)
                                                    .ConfigureAwait(false))
            {
                yield return new Event<T>(storedEvent.Timestamp, MemoryMarshal.Read<T>(storedEvent.Payload));
            }
        }

        /// <summary>
        ///     Creates a new track.
        /// </summary>
        /// <param name="genericTrack">The underlying generic track.</param>
        /// <param name="serializer">The serializer for the events.</param>
        internal Track(Track genericTrack, IEventSerializer serializer)
        {
            this.genericTrack = genericTrack;
            this.serializer   = serializer;
        }

        /// <summary>
        ///     The underlying generic track.
        /// </summary>
        private readonly Track            genericTrack;
        
        /// <summary>
        ///     The serializer for the events.
        /// </summary>
        private readonly IEventSerializer serializer;
    }
}

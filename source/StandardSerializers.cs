using System;
using System.Runtime.InteropServices;

namespace QTapeSharp
{
    /// <summary>
    ///     An event serializer for a 32-bit integer payload.
    /// </summary>
    public sealed class Int32Serializer : IEventSerializer
    {
        /// <inheritdoc />
        public Type EventType => typeof(int);
        
        /// <inheritdoc />
        public int EventSize => sizeof(int);

        /// <inheritdoc />
        public string TypeName => "Int32";

        /// <inheritdoc />
        public void Serialize(object payload, Span<byte> buffer)
        {
            if (buffer.Length < this.EventSize)
            {
                throw new Exception("The buffer is too small.");
            }

            if (!(payload is int value))
            {
                throw new Exception("Invalid type.");
            }

            MemoryMarshal.Write(buffer, ref value);
        }

        /// <inheritdoc />
        public object Deserialize(ReadOnlySpan<byte> data)
        {
            if (data.Length < this.EventSize)
            {
                throw new Exception("The amount of data is too small.");
            }

            return MemoryMarshal.Read<int>(data);
        }
    }

    /// <summary>
    ///     An event serializer for a 64-bit integer payload.
    /// </summary>
    public sealed class Int64Serializer : IEventSerializer
    {
        /// <inheritdoc />
        public Type EventType => typeof(long);
        
        /// <inheritdoc />
        public int EventSize => sizeof(long);

        /// <inheritdoc />
        public string TypeName => "Int64";

        /// <inheritdoc />
        public void Serialize(object payload, Span<byte> buffer)
        {
            if (buffer.Length < this.EventSize)
            {
                throw new Exception("The buffer is too small.");
            }

            if (!(payload is long value))
            {
                throw new Exception("Invalid type.");
            }

            MemoryMarshal.Write(buffer, ref value);
        }

        /// <inheritdoc />
        public object Deserialize(ReadOnlySpan<byte> data)
        {
            if (data.Length < this.EventSize)
            {
                throw new Exception("The amount of data is too small.");
            }

            return MemoryMarshal.Read<long>(data);
        }
    }

    /// <summary>
    ///     An event serializer for a single-precision floating-point number payload.
    /// </summary>
    public sealed class FloatSerializer : IEventSerializer
    {
        /// <inheritdoc />
        public Type EventType => typeof(float);
        
        /// <inheritdoc />
        public int EventSize => sizeof(float);

        /// <inheritdoc />
        public string TypeName => "Float";

        /// <inheritdoc />
        public void Serialize(object payload, Span<byte> buffer)
        {
            if (buffer.Length < this.EventSize)
            {
                throw new Exception("The buffer is too small.");
            }

            if (!(payload is float value))
            {
                throw new Exception("Invalid type.");
            }

            MemoryMarshal.Write(buffer, ref value);
        }

        /// <inheritdoc />
        public object Deserialize(ReadOnlySpan<byte> data)
        {
            if (data.Length < this.EventSize)
            {
                throw new Exception("The amount of data is too small.");
            }

            return MemoryMarshal.Read<float>(data);
        }
    }

    /// <summary>
    ///     An event serializer for a double-precision floating-point number payload.
    /// </summary>
    public sealed class DoubleSerializer : IEventSerializer
    {
        /// <inheritdoc />
        public Type EventType => typeof(double);
        
        /// <inheritdoc />
        public int EventSize => sizeof(double);

        /// <inheritdoc />
        public string TypeName => "Double";
        
        /// <inheritdoc />
        public void Serialize(object payload, Span<byte> buffer)
        {
            if (buffer.Length < this.EventSize)
            {
                throw new Exception("The buffer is too small.");
            }

            if (!(payload is double value))
            {
                throw new Exception("Invalid type.");
            }

            MemoryMarshal.Write(buffer, ref value);
        }

        /// <inheritdoc />
        public object Deserialize(ReadOnlySpan<byte> data)
        {
            if (data.Length < this.EventSize)
            {
                throw new Exception("The amount of data is too small.");
            }

            return MemoryMarshal.Read<double>(data);
        }
    }
    
    /// <summary>
    ///     An event serializer for a decimal floating-point number payload.
    /// </summary>
    public sealed class DecimalSerializer : IEventSerializer
    {
        /// <inheritdoc />
        public Type EventType => typeof(decimal);
        
        /// <inheritdoc />
        public int EventSize => sizeof(decimal);

        /// <inheritdoc />
        public string TypeName => "CSharpDecimal";

        /// <inheritdoc />
        public void Serialize(object payload, Span<byte> buffer)
        {
            if (buffer.Length < this.EventSize)
            {
                throw new Exception("The buffer is too small.");
            }

            if (!(payload is decimal value))
            {
                throw new Exception("Invalid type.");
            }

            MemoryMarshal.Write(buffer, ref value);
        }

        /// <inheritdoc />
        public object Deserialize(ReadOnlySpan<byte> data)
        {
            if (data.Length < this.EventSize)
            {
                throw new Exception("The amount of data is too small.");
            }

            return MemoryMarshal.Read<decimal>(data);
        }
    }
}

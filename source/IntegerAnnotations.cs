using System;
using Qubus.Errors;

namespace QTapeSharp
{
    /// <summary>
    ///     A set of code annotations for safe integer operations.
    /// </summary>
    public static class Integers
    {
        /// <summary>
        ///     Requires that the conversion to a 32-bit integer is safe, i.e., it does not loose any information.
        /// </summary>
        /// <param name="value">The original value.</param>
        /// <returns>the converted value.</returns>
        public static int RequireSafeConversionToInt32(long value)
        {
            Release.Assert(Int32.MinValue <= value && value <= Int32.MaxValue, "The conversion is not safe.");

            return (int) value;
        }

        /// <summary>
        ///     Requires that the conversion to a 32-bit integer is safe, i.e., it does not loose any information.
        /// </summary>
        /// <param name="value">The original value.</param>
        /// <returns>the converted value.</returns>
        public static int RequireSafeConversionToInt32(int value)
        {
            // This conversion is always safe.

            return value;
        }

        /// <summary>
        ///     Requires that the conversion to a 32-bit integer is safe, i.e., it does not loose any information.
        /// </summary>
        /// <param name="value">The original value.</param>
        /// <returns>the converted value.</returns>
        public static int RequireSafeConversionToInt32(short value)
        {
            // This conversion is always safe.

            return value;
        }
    }
}

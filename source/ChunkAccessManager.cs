﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Qubus.Concurrency;
using Qubus.Lifetime;

namespace QTapeSharp
{
    /// <summary>
    ///     A file chunk which is currently accessed by the library.
    /// </summary>
    internal sealed class ActiveChunk : IAsyncDisposable
    {
        /// <summary>
        ///     Creates a new active chunk from a file chunk.
        /// </summary>
        /// <param name="chunk">The underlying file chunk.</param>
        /// <param name="cachedData">The cached in-memory version of the underlying file chunk.</param>
        /// <param name="activeChunkTable">The active chunk table which lists this active chunk.</param>
        /// <param name="cache">The cache which is managing the cached data for this chunk.</param>
        public ActiveChunk(FileChunk chunk, InMemoryChunk cachedData, ActiveChunkTable activeChunkTable, ChunkCache cache)
        {
            this.chunk      = chunk;
            this.cachedData = cachedData;
            
            this.activeChunkTable = activeChunkTable;
            this.cache            = cache;
        }

        /// <summary>
        ///     The number of bytes of this chunk which are in use.
        /// </summary>
        public int UsedBytes => this.cachedData.UsedBytes;

        /// <summary>
        ///     The start offset of this chunk.
        /// </summary>
        public long StartOffset => this.chunk.StartOffset;

        /// <summary>
        ///     Calculates an absolute file offset from a relative offset for this chunk.
        /// </summary>
        /// <param name="relativeOffset">The relative offset with respect to this chunk.</param>
        /// <returns>the absolute file offset.</returns>
        public long CalculateAbsoluteOffset(long relativeOffset)
        {
            return this.chunk.CalculateAbsoluteOffset(relativeOffset);
        }

        /// <summary>
        ///     Queries the offset of the next chunk, if any.
        /// </summary>
        /// <returns>the offset of the next chunk, if any.</returns>
        public long? QueryNextChunkOffset()
        {
            return this.cachedData.QueryNextChunkOffset();
        }
        
        /// <summary>
        ///     Reads some bytes from this chunk.
        /// </summary>
        /// <remarks>
        ///     This routine will read at most the number of bytes which fit into the provided buffer.
        ///     Less bytes than this maximum might be read by a single call. The actual number is returned
        ///     by the routine.
        /// </remarks>
        /// <param name="offset">The relative start offset for the read.</param>
        /// <param name="buffer">The buffer into which the data is read.</param>
        /// <returns>the number of actually read bytes.</returns>
        public int Read(int offset, Span<byte> buffer)
        {
            return this.cachedData.Read(offset, buffer);
        }

        /// <summary>
        ///     Writes new data to this chunk.
        /// </summary>
        /// <remarks>
        ///     This routine will write at most the number of bytes which are provided by the buffer.
        ///     Less bytes than this maximum might be written by a single call. The actual number is returned
        ///     by the routine.
        /// </remarks>
        /// <param name="buffer">The buffer from which the data is written to the chunk.</param>
        /// <returns>the number of actually written bytes.</returns>
        public int Write(ReadOnlySpan<byte> buffer)
        {
            return this.cachedData.Write(buffer);
        }

        /// <summary>
        ///     Writes some data *atomically* to this chunk.
        /// </summary>
        /// <remarks>
        ///     This function is meant to be used if a write needs to be
        ///     atomic to ensure consistency.
        /// 
        ///     The atomicity guarantee means that a call with either write all
        ///     data to this chunk or none at all. If the data does not fit into
        ///     this chunk, a new chunk has to be allocated. If the amount of data is
        ///     larger than the overall size of a chunk, it is impossible to write
        ///     the data atomically at all since even an empty chunk could not contain it.
        ///     Calling code is responsible for taking this into account.
        /// </remarks>
        /// <param name="data">The buffer from which the data is written to the chunk.</param>
        /// <returns>true if the data has been written, false otherwise.</returns>
        public bool TryWriteAtomically(ReadOnlySpan<byte> data)
        {
            return this.cachedData.TryWriteAtomically(data);
        }

        /// <summary>
        ///     Finalizes this chunk.
        /// </summary>
        /// <remarks>
        ///     After a chunk has been finalized, is must not be modified anymore. Any new data
        ///     should be written to the next chunk.
        /// </remarks>
        /// <param name="nextChunkOffset">The absolute file offset of the following chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the handle for this asynchronous operation.</returns>
        public async ValueTask Finalize(long nextChunkOffset, CancellationToken cancellationToken)
        {
            this.cachedData.Finalize(nextChunkOffset);

            await UpdatePersistentHeader(cancellationToken);
        }

        /// <summary>
        ///     Updates the persistent version of the chunk header.
        /// </summary>
        /// <remarks>
        ///     This routine will not ensure that any chunk payload has actually been written
        ///     to the persistent storage. This should be ensured by other means
        ///     prior to calling this function.
        /// </remarks>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the handle for this asynchronous operation.</returns>
        public ValueTask UpdatePersistentHeader(CancellationToken cancellationToken)
        {
            return this.cache.FlushChunk(this.chunk.StartOffset, cancellationToken);
        }

        /// <inheritdoc />
        public ValueTask DisposeAsync()
        {
            return this.activeChunkTable.ReleaseActiveChunk(this.chunk.StartOffset, CancellationToken.None);
        }

        /// <summary>
        ///     The underlying file chunk.
        /// </summary>
        private readonly FileChunk chunk;
        
        private readonly InMemoryChunk cachedData;

        /// <summary>
        ///     The active chunk table which lists this active chunk.
        /// </summary>
        private readonly ActiveChunkTable activeChunkTable;

        private readonly ChunkCache cache;
    }

    /// <summary>
    ///     The central management object for all active chunks of a specific file.
    /// </summary>
    internal sealed class ChunkAccessManager : IDisposable
    {
        /// <summary>
        ///     Creates a new chunk manager.
        /// </summary>
        /// <remarks>
        ///     The allocator must operate on the same file as the created chunk access manager. This
        ///     is currently *not* checked.
        /// </remarks>
        /// <param name="file">The file for which the chunks are managed.</param>
        /// <param name="allocator">The allocator which is used to allocate new chunks if necessary.</param>
        public ChunkAccessManager(RandomAccessFile file, FileChunkAllocator allocator)
        {
            this.file      = file;
            this.allocator = allocator;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.allocator.Dispose();
        }

        /// <summary>
        ///     Allocates a new chunk.
        /// </summary>
        /// <param name="size">The size of the chunk in bytes, including all metadata.</param>
        /// <param name="alignment">The alignment of the chunk in bytes.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the new chunk.</returns>
        public async ValueTask<AsyncArc<ActiveChunk>> Allocate(int               size,
                                                               long              alignment,
                                                               CancellationToken cancellationToken)
        {
            FileChunk newChunk =
                await this.allocator.Allocate(size, alignment, cancellationToken).ConfigureAwait(false);

            return await this.activeChunkTable.AddActiveChunk(newChunk, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        ///     Opens an existing chunk.
        /// </summary>
        /// <param name="offset">The absolute starting offset of the chunk within the file.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the handle for the opened chunk.</returns>
        public async ValueTask<AsyncArc<ActiveChunk>> Open(long offset, CancellationToken cancellationToken)
        {
            AsyncArc<ActiveChunk>? activeChunk =
                await this.activeChunkTable.LookupActiveChunk(offset, cancellationToken).ConfigureAwait(false);

            if (activeChunk != null)
            {
                return activeChunk;
            }

            var newChunk = await FileChunk.Open(this.file, offset, cancellationToken).ConfigureAwait(false);

            return await this.activeChunkTable.AddActiveChunk(newChunk, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        ///     Queries the next chunk, if any, for a specified chunk.
        /// </summary>
        /// <param name="currentChunk">The chunk for which the next chunk should be queried.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the handle for the opened chunk, or null if none exists.</returns>
        public async ValueTask<AsyncArc<ActiveChunk>?> QueryNextChunk(ActiveChunk       currentChunk,
                                                                      CancellationToken cancellationToken)
        {
            long? nextChunkOffset = currentChunk.QueryNextChunkOffset();

            if (nextChunkOffset == null)
            {
                return null;
            }

            return await Open(nextChunkOffset.Value, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        ///     The file for which the chunks are managed.
        /// </summary>
        private readonly RandomAccessFile   file;
        
        /// <summary>
        ///     The allocator which is used to allocate new chunks if necessary.
        /// </summary>
        private readonly FileChunkAllocator allocator;

        /// <summary>
        ///     The table of all currently active chunks.
        /// </summary>
        private readonly ActiveChunkTable activeChunkTable = new ActiveChunkTable();
    }

    /// <summary>
    ///     A table of active chunks.
    /// </summary>
    internal sealed class ActiveChunkTable
    {
        /// <summary>
        ///     Adds an active chunk to the table.
        /// </summary>
        /// <param name="chunk">The new chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the handle for the new active chunk.</returns>
        public async ValueTask<AsyncArc<ActiveChunk>> AddActiveChunk(FileChunk         chunk,
                                                                     CancellationToken cancellationToken)
        {
            using var guard = await this.activeChunkIndex.Acquire(cancellationToken).ConfigureAwait(false);

            InMemoryChunk cachedData = await this.cache.CacheChunk(chunk, cancellationToken);
            
            Dictionary<long, AsyncArc<ActiveChunk>.WeakReference> activeChunks = guard.Value;

            var newActiveChunk = new AsyncArc<ActiveChunk>(new ActiveChunk(chunk, cachedData, this, this.cache));

            activeChunks.Add(chunk.StartOffset, new AsyncArc<ActiveChunk>.WeakReference(newActiveChunk));

            return newActiveChunk;
        }

        /// <summary>
        ///     Looks up an active chunk by the chunk's offset.
        /// </summary>
        /// <param name="offset">The offset of the chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the active chunk, if any, or null otherwise.</returns>
        public async ValueTask<AsyncArc<ActiveChunk>?> LookupActiveChunk(
            long              offset,
            CancellationToken cancellationToken)
        {
            using var guard = await this.activeChunkIndex.Acquire(cancellationToken).ConfigureAwait(false);

            Dictionary<long, AsyncArc<ActiveChunk>.WeakReference> activeChunks = guard.Value;

            if (activeChunks.TryGetValue(offset, out AsyncArc<ActiveChunk>.WeakReference? activeChunk))
            {
                return activeChunk.Lock();
            }

            return null;
        }

        /// <summary>
        ///     Releases an active chunk.
        /// </summary>
        /// <param name="offset">The offset of the chunk.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the handle for the asynchronous operation.</returns>
        public async ValueTask ReleaseActiveChunk(long offset, CancellationToken cancellationToken)
        {
            using var guard = await this.activeChunkIndex.Acquire(cancellationToken).ConfigureAwait(false);

            Dictionary<long, AsyncArc<ActiveChunk>.WeakReference> activeChunks = guard.Value;

            activeChunks.Remove(offset);
            
            // Evict the chunk from the cache since we will most likely not touch it any time soon.
            await this.cache.EvictChunk(offset, cancellationToken);
        }

        /// <summary>
        ///     The internal thread-safe table of active chunks.
        /// </summary>
        private readonly SynchronizedValue<Dictionary<long, AsyncArc<ActiveChunk>.WeakReference>> activeChunkIndex =
            new SynchronizedValue<Dictionary<long, AsyncArc<ActiveChunk>.WeakReference>>(
                new Dictionary<long, AsyncArc<ActiveChunk>.WeakReference>());
        
        /// <summary>
        ///     The in-memory chunk cache.
        /// </summary>
        private readonly ChunkCache cache = new ChunkCache();
    }
}
